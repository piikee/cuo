﻿#ifndef CUOUSERWINDOW_H
#define CUOUSERWINDOW_H


#include <QObject>
#include <QWidget>
#include <QJsonDocument>
#include <QList>
#include <QSlider>
#include <QDesktopServices>//piikee 20141202 新窗口打开广告或公告
#include <QSystemTrayIcon>
namespace Ui {
class CuoUserWindow;
}
class QStandardItemModel;
class QStandardItem;

class CuoUserWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CuoUserWindow(QWidget *parent = 0);
    ~CuoUserWindow();
	QSystemTrayIcon *trayIcon; 
    void refreshUserInfo();
	void refreshVipInfo(const QString& msg);
	void refreshTaskInfo(const QString&);
  /*  void commitTaskUrl();
    void commitTaskTaobao();*/
    void commitTaskBaidu();
	void commitTaskBaidu_m();
	void commitTaskBaidu_click();
	/*void commitTaskAlibaba();
	void commitTaskEtao();*/
	void cancelTask();
	void vipQueryUser();
	void vipAddScore();
	void vipMinScore();
	QIcon icon;
static	void writeConfig(QString info);
static	  QStringList readConfig();
void CuoUserWindow::changeEvent(QEvent *e); 
 
private:
   /* void _commitTaskUrl();
    void _commitTaskTaobao();*/
    void _commitTaskBaidu();
	void _commitTaskBaidu_m();
	void _commitTaskBaidu_click();
	/*void _commitTaskAlibaba();
	void _commitTaskEtao();*/
    void _cancelTask();
    
public slots:
   /* void onCommitTaskUrl();
    void onCommitTaskTaobao();*/
	void onCommitTaskBaidu();
	void onCommitTaskBaidu_m();
	void onCommitTaskBaidu_click();

	/*void onCommitTaskAlibaba();
	void onCommitTaskEtao();*/
	void onMyTasksClicked(const QModelIndex &index);
    void onCancelTaskClicked();
	void onSaveButtonClicked();
	void openADsUrl(const QUrl &url);//piikee 20141202 新窗口打开广告或公告
	void onSystemTrayIconClicked(QSystemTrayIcon::ActivationReason reason);

	void onVipQueryUser();
	void onVipAddScore();
	void onVipMinScore();

private:
    Ui::CuoUserWindow *ui;
   /* QList<QSlider*>     m_sliderList_url;
    QList<QSlider*>     m_sliderList_taobao;*/
    QList<QSlider*>     m_sliderList_baidu;
	QList<QSlider*>     m_sliderList_baidu_m;
	QList<QSlider*>     m_sliderList_baidu_click;

	/*QList<QSlider*>     m_sliderList_alibaba;*/
	/*QList<QSlider*>     m_sliderList_etao;*/
    QStandardItemModel* m_myTasksModel;
    int                 m_tasksModelRowCount;
    QStandardItem     * m_selectedItem;
	
};

#endif // CUOUSERWINDOW_H
