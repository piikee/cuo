﻿#include <QJsonDocument>
#include <QJsonObject>
#include "cuoproto.h"

#include <QDebug>


#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

void d(QString a)
{
    qDebug()<<"d: "<<a;
}

int IProto::serialize(QString& output)
{
    Q_UNUSED(output);
    return 0;
}

int IProto::unserialize(const QString& input)
{
    Q_UNUSED(input);
    return 0;
}

//---------------------------------
DEFINE_PROTO_IMP_BEGIN(login)
DEFINE_PROTO_FIELD_IMP(login, name)
DEFINE_PROTO_FIELD_IMP(login, password)
DEFINE_PROTO_IMP_END(login)

DEFINE_PROTO_IMP_BEGIN(logout)
DEFINE_PROTO_FIELD_IMP(logout, title)
DEFINE_PROTO_FIELD_IMP(logout, msg)
DEFINE_PROTO_IMP_END(logout)

//---------------------------------
DEFINE_PROTO_IMP_BEGIN(register)
DEFINE_PROTO_FIELD_IMP(register, name)
DEFINE_PROTO_FIELD_IMP(register, group)
DEFINE_PROTO_FIELD_IMP(register, password)
DEFINE_PROTO_FIELD_IMP(register,qq)
DEFINE_PROTO_FIELD_IMP(register,email)
DEFINE_PROTO_FIELD_IMP(register,tel)
DEFINE_PROTO_IMP_END(register)
//---------------------------------

DEFINE_PROTO_IMP_BEGIN(heartbeat)
DEFINE_PROTO_IMP_END(heartbeat)

DEFINE_PROTO_IMP_BEGIN(unlock)
DEFINE_PROTO_IMP_END(unlock)

DEFINE_PROTO_IMP_BEGIN(message)
DEFINE_PROTO_FIELD_IMP(message, title)
DEFINE_PROTO_FIELD_IMP(message, msg)
DEFINE_PROTO_IMP_END(message)

DEFINE_PROTO_IMP_BEGIN(user)
DEFINE_PROTO_FIELD_IMP(user, name)
DEFINE_PROTO_FIELD_IMP(user, ip)
DEFINE_PROTO_FIELD_IMP(user, score)
DEFINE_PROTO_FIELD_IMP(user, role)
DEFINE_PROTO_FIELD_IMP(user, today_score)
DEFINE_PROTO_IMP_END(user)


DEFINE_PROTO_IMP_BEGIN(labor)
DEFINE_PROTO_FIELD_IMP(labor, content)
DEFINE_PROTO_FIELD_IMP(labor, key)
DEFINE_PROTO_IMP_END(labor)


DEFINE_PROTO_IMP_BEGIN(report)
DEFINE_PROTO_FIELD_IMP(report, key)
DEFINE_PROTO_FIELD_IMP(report, proxy_ip)
DEFINE_PROTO_FIELD_IMP(report, result)
DEFINE_PROTO_FIELD_IMP(report, success)
DEFINE_PROTO_IMP_END(report)


DEFINE_PROTO_IMP_BEGIN(task)
DEFINE_PROTO_FIELD_IMP(task, type)
DEFINE_PROTO_FIELD_IMP(task, start_day)
DEFINE_PROTO_FIELD_IMP(task, duration_day)
DEFINE_PROTO_FIELD_IMP(task, time_distribution)
DEFINE_PROTO_FIELD_IMP(task, day_ip_count)
DEFINE_PROTO_FIELD_IMP(task, pv_raito)
DEFINE_PROTO_FIELD_IMP(task, url)
DEFINE_PROTO_FIELD_IMP(task, ua)
DEFINE_PROTO_FIELD_IMP(task, referer)
DEFINE_PROTO_FIELD_IMP(task, keyword)
DEFINE_PROTO_FIELD_IMP(task, keyword_b)
DEFINE_PROTO_FIELD_IMP(task, shop)
DEFINE_PROTO_IMP_END(task)

DEFINE_PROTO_IMP_BEGIN(mytask)
DEFINE_PROTO_FIELD_IMP(mytask, key)
DEFINE_PROTO_FIELD_IMP(mytask, type)
DEFINE_PROTO_FIELD_IMP(mytask, start_day)
DEFINE_PROTO_FIELD_IMP(mytask, end_day)
DEFINE_PROTO_FIELD_IMP(mytask, keyword)
DEFINE_PROTO_FIELD_IMP(mytask, keyword_b)
DEFINE_PROTO_FIELD_IMP(mytask, shop)
DEFINE_PROTO_FIELD_IMP(mytask, day_ip_count)
DEFINE_PROTO_FIELD_IMP(mytask, total_completed_count)
DEFINE_PROTO_FIELD_IMP(mytask, day_completed_ip_count)
DEFINE_PROTO_IMP_END(mytask)

DEFINE_PROTO_IMP_BEGIN(cancel)
DEFINE_PROTO_FIELD_IMP(cancel, key)
DEFINE_PROTO_IMP_END(cancel)


DEFINE_PROTO_IMP_BEGIN(queryuser)
DEFINE_PROTO_FIELD_IMP(queryuser, name)
DEFINE_PROTO_IMP_END(queryuser)

DEFINE_PROTO_IMP_BEGIN(queryresult)
DEFINE_PROTO_FIELD_IMP(queryresult, score)
DEFINE_PROTO_FIELD_IMP(queryresult, error)
DEFINE_PROTO_IMP_END(queryresult)

DEFINE_PROTO_IMP_BEGIN(addscore)
DEFINE_PROTO_FIELD_IMP(addscore, name)
DEFINE_PROTO_FIELD_IMP(addscore, score)
DEFINE_PROTO_IMP_END(addscore)

DEFINE_PROTO_IMP_BEGIN(iamproxy)
DEFINE_PROTO_FIELD_IMP(iamproxy, ip)
DEFINE_PROTO_IMP_END(iamproxy)

DEFINE_PROTO_IMP_BEGIN(expireproxy)
DEFINE_PROTO_IMP_END(expireproxy)

