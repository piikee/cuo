﻿//
//  cuouser.h
//  Core
//
//  Created by Yiwei Wang on 11/2/14.
//
//

#ifndef __Core__cuouser__
#define __Core__cuouser__

#include <QObject>
#include <QMap>

//------------------------------------------------

class CuoUserData : public QObject
{
    Q_OBJECT
    
public:
    static CuoUserData* getInstance();
    
    static void Release();
    
    virtual void init();
    
    virtual bool isInited() const;
    
    void setData(const QString& name, const QString& score, const QString& today_score, const QString& ip, const QString& role);
    QString getName();
    QString getScore();
    QString getTodayScore();
    QString getIp();
	QString getRole();
    
private:
    explicit CuoUserData();
    virtual ~CuoUserData();
    
    
private slots:

private:
    static CuoUserData* pInstance;
    bool m_inited;
    
    QString     m_name;
    QString     m_score;
    QString     m_today_score;
    QString     m_ip;
	QString		m_role;
};



#endif /* defined(__Core__cuouser__) */
