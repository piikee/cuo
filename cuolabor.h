﻿#ifndef CUOLABOR_H
#define CUOLABOR_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMap>

class ILaborStep
{
public:
    virtual const char* getType() = 0;
    virtual ~ILaborStep(){};
	virtual int getSteptime() const;
	virtual void setSteptime(int t);
	virtual int getStepcount() const;
	virtual void setStepcount(int t);
	virtual int getIgnoreCompletedEvent() const;
	virtual void setIgnoreCompletedEvent(int t);
protected:
    int     m_steptime;
	int		m_stepcount;
	int		m_ignoreCompletedEvent;
};

class LaborURL : public ILaborStep
{
public:
    const char* getType();
    QString Url;
    QString Ua;
    QMap<QString, QString> Header;
};

class LaborJAVASCRIPT : public ILaborStep
{
public:
    const char* getType();
    QString Script;
};

class CuoLabor
{
public:
    CuoLabor(const QString& key, const QString& data);
    bool isOK() const;
    int getInittime() const;
    int getTotaltime() const;
    int getStepCount() const;
    ILaborStep* getStep(int s);
    
    void Report(const QString&);
    void Report(const QString&, const QString&, const QString&);
    ~CuoLabor();
private:
    bool    m_ok;
    QString m_key;
    int     m_inittime;
    int     m_totaltime;
    QList<ILaborStep*> m_steps;
};

#endif // CUOLABOR_H
