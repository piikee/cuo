﻿#include <Qt>
#include <QtWidgets>
#include <QtWebKitWidgets>

#include "cuowebview.h"


#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

//#include "cuowebaction.h"
///
/// \brief CuoWebPage::CuoWebPage
/// \param parent
///
CuoWebPage::CuoWebPage(QWidget *parent)
:QWebPage(parent)
{
}

void CuoWebPage::setUserAgent(const QString &url)
{
	this->useragent = url;
}

QString CuoWebPage::userAgentForUrl(const QUrl &url) const
{
	Q_UNUSED(url)
		return this->useragent;
}
void CuoWebPage::javaScriptAlert(QWebFrame* frame, const QString & msg)
{
	Q_UNUSED(frame)
		qDebug() << "javaScriptAlert: " << msg;
}
bool CuoWebPage::javaScriptConfirm(QWebFrame* frame, const QString & msg)
{
	Q_UNUSED(frame)
		qDebug() << "javaScriptConfirm: " << msg;
	return true;
}
void CuoWebPage::javaScriptConsoleMessage(const QString & message, int lineNumber, const QString & sourceID)
{
	qDebug() << "javaScriptConsoleMessage: " << message << lineNumber << sourceID;
}
bool CuoWebPage::javaScriptPrompt(QWebFrame * frame, const QString & msg, const QString & defaultValue, QString * result)
{
	Q_UNUSED(frame)
		qDebug() << "javaScriptPrompt: " << msg << defaultValue << result;
	return false;
}


///
/// \brief The CuoCookieJar class
///
CuoCookieJar::CuoCookieJar(QObject *parent)
:QNetworkCookieJar(parent)
{
	load();
}
CuoCookieJar::~CuoCookieJar()
{
	save();
}

void CuoCookieJar::save()
{
	QList<QNetworkCookie> list = allCookies();
	QByteArray data;
	foreach(QNetworkCookie cookie, list)
	{
		if (!cookie.isSessionCookie())
		{
			data.append(cookie.toRawForm());
			data.append("\n");
		}
	}
	qDebug() << "Cookies: " << data;
}

void CuoCookieJar::load()
{
}

void CuoCookieJar::clean()
{
	QList<QNetworkCookie> empty;
	setAllCookies(empty);
}


///
/// \brief CuoWebView::CuoWebView
/// \param parent
///
CuoWebView::CuoWebView(QWidget *parent)
:QWebView(parent)
{
	m_webpage = new CuoWebPage(this);
	this->setPage(m_webpage);
	 QWebSettings *websettings = m_webpage->settings();
	 websettings->enablePersistentStorage(QDir::homePath());
	 websettings->setAttribute(QWebSettings::LocalStorageEnabled, true);
 
	 websettings->setAttribute(QWebSettings::OfflineWebApplicationCacheEnabled, true);
 websettings->setOfflineWebApplicationCachePath(QDir::homePath());


	m_cookieJar = new CuoCookieJar(this);
	m_networkAccessManager = new QNetworkAccessManager(this);
	m_networkAccessManager->setCookieJar(m_cookieJar); 

	m_cookieJar = new CuoCookieJar(this);
	 m_diskCache = new QNetworkDiskCache(this);
	 m_diskCache->setMaximumCacheSize(100 * 1024 * 1024);  // 100MB cache
	 m_diskCache->setCacheDirectory("cuocache");

	m_networkAccessManager = new QNetworkAccessManager(this);
	m_networkAccessManager->setCookieJar(m_cookieJar);
	 m_networkAccessManager->setCache(m_diskCache);
	 
		page()->setNetworkAccessManager(m_networkAccessManager);
	 page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);//Handle link clicks by yourself-------
	//page()->setContextMenuPolicy(Qt::NoContextMenu); //No context menu is allowed if you don't need it
	 connect(this, SIGNAL(linkClicked(QUrl)), this, SLOT(linkClickedSlot(QUrl)));//------
	m_inputTimer = new QTimer();
	connect(m_inputTimer, SIGNAL(timeout()), this, SLOT(_input()));
	this->setContextMenuPolicy(Qt::NoContextMenu);
}

void CuoWebView::setProxy(const QNetworkProxy& proxy)
{
	m_networkAccessManager->setProxy(proxy);
}
void CuoWebView::load(const QNetworkRequest &request)
{
	page()->mainFrame()->load(request);
}

void CuoWebView::setUserAgent(const QString &url)
{
	m_webpage->setUserAgent(url);
}

void CuoWebView::cleanCookie()
{
	m_cookieJar->clean();
}
QWebView * CuoWebView::createWindow(QWebPage::WebWindowType type)
{
	QWebView *webView = new QWebView;
	QWebPage *newWeb = new QWebPage(webView);
	webView->setAttribute(Qt::WA_DeleteOnClose, true);
	webView->setPage(newWeb);
	webView->show();
	return webView;
	qDebug() << "Create Windows: " << type;
	return this;
}
//This slot handles all clicks
void CuoWebView::linkClickedSlot(QUrl url)
{
	QNetworkRequest request = QNetworkRequest(url);
	request.setRawHeader("Referer", this->url().toEncoded());
	qDebug() << "handleLinkClicked: " << url.toString() << " referer: " << this->url();
	load(request);
//	QWebView::load(url);
}


void CuoWebView::click(const QString &selectorQuery)
{
	QWebElement el = this->page()->mainFrame()->findFirstElement(selectorQuery);
	//if (!el)
	//     return;

	el.setFocus();
	QRect elGeom = el.geometry();
	QPoint elPoint = elGeom.center();
	int elX = elPoint.x();
	int elY = elPoint.y();
	qDebug() << "element " << selectorQuery << " X: " << elX << " Y: " << elY;
	int webWidth = this->width();
	int webHeight = this->height();
	int pixelsToScrolRight = 0;
	int pixelsToScrolDown = 0;
	if (elX>webWidth) pixelsToScrolRight = elX - webWidth + elGeom.width() / 2 + 10; //the +10 part if for the page to scroll a bit further
	if (elY>webHeight) pixelsToScrolDown = elY - webHeight + elGeom.height() / 2 + 10; //the +10 part if for the page to scroll a bit further
	this->page()->mainFrame()->setScrollBarValue(Qt::Horizontal, pixelsToScrolRight);
	this->page()->mainFrame()->setScrollBarValue(Qt::Vertical, pixelsToScrolDown);
	QPoint pointToClick(elX - pixelsToScrolRight, elY - pixelsToScrolDown);

	QMouseEvent *pressEvent = new QMouseEvent(QMouseEvent::MouseButtonPress, pointToClick, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QCoreApplication::postEvent(this, pressEvent);
	QMouseEvent *releaseEvent = new QMouseEvent(QMouseEvent::MouseButtonRelease, pointToClick, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QCoreApplication::postEvent(this, releaseEvent);
}

void CuoWebView::click(const QPoint& elPoint)
{
	//QWebElement el = this->page()->mainFrame()->findFirstElement(selectorQuery);
	//if (!el)
	//     return;

	//el.setFocus();
	//QRect elGeom = el.geometry();
	//QPoint elPoint = elGeom.center();
	int elX = elPoint.x();
	int elY = elPoint.y();
	int webWidth = this->width();
	int webHeight = this->height();
	int pixelsToScrolRight = 0;
	int pixelsToScrolDown = 0;
	if (elX>webWidth) pixelsToScrolRight = elX - webWidth / 2; //the +10 part if for the page to scroll a bit further
	if (elY>webHeight) pixelsToScrolDown = elY - webHeight / 2; //the +10 part if for the page to scroll a bit further
	this->page()->mainFrame()->setScrollBarValue(Qt::Horizontal, pixelsToScrolRight);
	this->page()->mainFrame()->setScrollBarValue(Qt::Vertical, pixelsToScrolDown);
	QPoint pointToClick(elX - pixelsToScrolRight, elY - pixelsToScrolDown);

	QMouseEvent *pressEvent = new QMouseEvent(QMouseEvent::MouseButtonPress, pointToClick, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QCoreApplication::postEvent(this, pressEvent);
	QMouseEvent *releaseEvent = new QMouseEvent(QMouseEvent::MouseButtonRelease, pointToClick, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QCoreApplication::postEvent(this, releaseEvent);
}

void CuoWebView::input(const QString& text)
{
	qDebug() << "CuoWebView input: " << text;
	m_inputText = text;
	m_inputTimer->start(100);
}
void CuoWebView::stopInput()
{
	qDebug() << "CuoWebView stop input ";
	m_inputTimer->stop();
}
void CuoWebView::_input()
{
	m_inputTimer->stop();
	if (m_inputText.count() <= 0)
	{
		emit sigInputCompleted();
		return;
	}
	QString s = m_inputText.left(1);
	m_inputText.remove(0, 1);
	QKeyEvent *pressEvent = new QKeyEvent(QKeyEvent::KeyPress, 0, 0, s);
	QKeyEvent *releaseEvent = new QKeyEvent(QKeyEvent::KeyRelease, 0, 0, s);
	QCoreApplication::postEvent(this, pressEvent);
	QCoreApplication::postEvent(this, releaseEvent);
	m_inputTimer->start(qrand() % 1500 + 100);
}