﻿#ifndef XOR_H
#define XOR_H

#include <QObject>


class Xor : public QObject
{
    Q_OBJECT

public:
    static QByteArray convertData(QByteArray key, QByteArray text);

private:
    Xor(QObject *parent = 0);

};

#endif // XOR_H
