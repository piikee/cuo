﻿#ifndef CUOWEBVIEW_H
#define CUOWEBVIEW_H

#include <QtWidgets>
#include <QtWebKitWidgets>
#include <QtWebKitDepends>

class QWebView;
class CuoCookieJar;
class QTimer;

class CuoWebPage : public QWebPage
{
	Q_OBJECT
public:
	CuoWebPage(QWidget *parent = 0);
	QString userAgentForUrl(const QUrl & url) const;
	void setUserAgent(const QString & url);

protected:
	void javaScriptAlert(QWebFrame*, const QString &);
	bool javaScriptConfirm(QWebFrame* frame, const QString & msg);
	void javaScriptConsoleMessage(const QString & message, int lineNumber, const QString & sourceID);
	bool javaScriptPrompt(QWebFrame * frame, const QString & msg, const QString & defaultValue, QString * result);
private:
	QString useragent;
};


class CuoCookieJar : public QNetworkCookieJar
{
	Q_OBJECT
public:
	CuoCookieJar(QObject *parent = 0);
	~CuoCookieJar();

public:
	void save();
	void load();
	void clean();
};

class CuoWebView : public QWebView
{
	Q_OBJECT

public:
	CuoWebView(QWidget *parent = 0);
	void setProxy(const QNetworkProxy& proxy);
	void load(const QNetworkRequest &url);
	void setUserAgent(const QString & url);
	void cleanCookie();
	void click(const QString &selectorQuery);
	void click(const QPoint& elGeom);
	void input(const QString& text);
	void stopInput();
signals:
	void sigInputCompleted();

private:

protected:
	QWebView * createWindow(QWebPage::WebWindowType type);
	protected slots:
	void linkClickedSlot(QUrl url);
	void _input();

private:
	CuoWebPage *            m_webpage;
	CuoCookieJar *          m_cookieJar;
	QNetworkDiskCache *		m_diskCache;
	QNetworkAccessManager * m_networkAccessManager;
	QTimer*					m_inputTimer;
	QString					m_inputText;
};

#endif // CUOWEBVIEW_H
