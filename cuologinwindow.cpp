﻿#include "cuologinwindow.h"
#include "ui_cuologinwindow.h"
#include "QtWidgets"
#include "cuonetio.h"
#include "cuoproto.h"
#include "cuouserwindow.h"

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

CuoLoginWindow::CuoLoginWindow(QWidget *parent)
:QWidget(parent),
    ui(new Ui::CuoLoginWindow)
{
    ui->setupUi(this);
    ui->checkBox->hide();
//	setWindowIcon(QIcon("ico.ico"));

    connect(ui->pushButtonLogin, SIGNAL(clicked()), this, SLOT(onLoginClick()));
    connect(ui->pushButtonRegister, SIGNAL(clicked()), this, SLOT(_onRegisterButtonClick()));

	//按钮按enter自动登录
	ui->pushButtonLogin->setShortcut(QKeySequence::InsertParagraphSeparator); //piikee 20141201
	ui->pushButtonLogin->setFocus(); //piikee 20141201
	ui->pushButtonLogin->setDefault(true);//piikee 20141201
	CuoLoginWindow::startconfig();
	 
    //this->setStyleSheet( "QGraphicsView { border-style: none; background: transparent}" );
    //this->setWindowFlags( Qt::Window |  Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint );
}

CuoLoginWindow::~CuoLoginWindow()
{
    delete ui;
}

void CuoLoginWindow::displayError(QString title, QString msg)
{
    QMessageBox::information(this, title, msg);
}

void CuoLoginWindow::onLoginClick()
{
    m_name = ui->lineEdit->text();
    m_password = ui->lineEdit_2->text();
    if (m_name.size()<3||m_name.size()>10)
    {
        return displayError("登陆失败", "请正确输入用户名");
    }
    if (m_password.size()<3||m_password.size()>20)
    {
        return displayError("登陆失败", "请正确输入密码");
    }
	savenamepwd();
    this->setEnabled(false);

    _login();
}

void CuoLoginWindow::_login()
{
    PROTO_NAME(login) loginData;
    loginData.name = m_name;
    loginData.password = m_password.toLatin1().toBase64();
    CuoNetIO::getInstance()->send(&loginData);

    emit aboutToLogin();
}

void CuoLoginWindow::tryLogin()
{
    if (m_name.isEmpty() || m_password.isEmpty()) {
        return;
    }
    _login();
}
void CuoLoginWindow::_onRegisterButtonClick()
{
    this->hide();
    emit aboutToRegister();
}
void CuoLoginWindow::startconfig()
{
	QStringList configstr =CuoUserWindow::readConfig();
	if (!configstr.isEmpty())
	{
		QString username, password, autologin, autostart;
		username.append(QByteArray::fromBase64(configstr[0].trimmed().toUtf8()));
		password.append(QByteArray::fromBase64(configstr[1].trimmed().toUtf8()));
		autologin.append(QByteArray::fromBase64(configstr[2].trimmed().toUtf8()));
		autostart.append(QByteArray::fromBase64(configstr[3].trimmed().toUtf8()));
		ui->lineEdit->setText(username.trimmed());
		ui->lineEdit_2->setText(password.trimmed());
			if (autologin == "true"){
				ui->pushButtonLogin->click();
				 
			}
			if (autostart == "true")
				setAutoStart(true);
			else
				setAutoStart(false);

	}

}

void CuoLoginWindow::setAutoStart(bool is_auto_start)
{
	QString application_name = QApplication::applicationName();
	QSettings *settings = new QSettings(REG_RUN, QSettings::NativeFormat);
	if (is_auto_start)
	{
		QString application_path = QApplication::applicationFilePath();
		settings->setValue(application_name, application_path.replace("/", "\\"));
	}
	else
	{
		settings->remove(application_name);
	}
	delete settings;
}

void CuoLoginWindow::savenamepwd()
{
	QStringList configstr = CuoUserWindow::readConfig();
	if (!configstr.isEmpty())
	{
		QString username, password, autologin, autostart; 
		username = ui->lineEdit->text().trimmed().toLatin1().toBase64();
		password = ui->lineEdit_2->text().trimmed().toLatin1().toBase64();
		autologin=configstr[2].trimmed();
		autostart=configstr[3].trimmed();
		CuoUserWindow::writeConfig(username + "\r\n" + password + "\r\n" + autologin + "\r\n" + autostart);

	}
	else
	{
		QString username, password, autologin, autostart;
		username = ui->lineEdit->text().trimmed().toLatin1().toBase64();;
		password = ui->lineEdit_2->text().trimmed().toLatin1().toBase64();;
		autologin = QString("false").toLatin1().toBase64();
		autostart = QString("false").toLatin1().toBase64();
		CuoUserWindow::writeConfig(username + "\r\n" + password + "\r\n" + autologin + "\r\n" + autostart);
	}
}