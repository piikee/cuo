﻿#include "cuouserwindow.h"
#include "ui_CuoUserWindow.h"
#include <QStandardItemModel>
#include <QPushButton>
#include <QGridLayout>
#include <QJsonObject>
#include <QJsonArray>
#include <QSignalMapper>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include "cuouser.h"
#include "cuonetio.h"
#include "cuotask.h"
#include <qstring.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qtextcodec.h> 
#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

CuoUserWindow::CuoUserWindow(QWidget *parent)
: QWidget(parent)
, ui(new Ui::CuoUserWindow)
, m_myTasksModel(NULL)
, m_tasksModelRowCount(0)
, m_selectedItem(NULL)
{
    ui->setupUi(this); 
	setWindowTitle("萍客电商营销宝 V6"); 
	ui->label_24->setVisible(false);
	ui->label_29->setVisible(false);
	ui->spinBox_pv_raito_3->setVisible(false);
	ui->spinBox_pv_raito_2->setVisible(false);
	 	//setWindowIcon(QIcon("Icon/ico.ico"));
     ui->tabWidget->removeTab(9);//隐藏vip界面的

	 ui->tabWidget->removeTab(5);
	 ui->tabWidget->removeTab(4);
	 ui->tabWidget->removeTab(3);
	 ui->tabWidget->removeTab(2);

	ui->tab_partner->setStyleSheet("tab{height:20px; width:160px; font: 8pt;alignment: left;}");

	ui->adview->load(QUrl("http://www.ruohuai.com/dsyxb/adviewv6.php"));//piikee 20141202 新窗口打开广告或公告
	ui->adview->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);//piikee 20141202 新窗口打开广告或公告
	connect(ui->adview, SIGNAL(linkClicked(const QUrl&)), this, SLOT(openADsUrl(const QUrl&)));//piikee 20141202 新窗口打开广告或公告

   /* m_sliderList_url.append(ui->verticalSlider_01);
    m_sliderList_url.append(ui->verticalSlider_02);
    m_sliderList_url.append(ui->verticalSlider_03);
    m_sliderList_url.append(ui->verticalSlider_04);
    m_sliderList_url.append(ui->verticalSlider_05);
    m_sliderList_url.append(ui->verticalSlider_06);
    m_sliderList_url.append(ui->verticalSlider_07);
    m_sliderList_url.append(ui->verticalSlider_08);
    m_sliderList_url.append(ui->verticalSlider_09);
    m_sliderList_url.append(ui->verticalSlider_10);
    m_sliderList_url.append(ui->verticalSlider_11);
    m_sliderList_url.append(ui->verticalSlider_12);
    for(auto slider = m_sliderList_url.begin(); slider != m_sliderList_url.end(); ++slider)
    {
        (*slider)->setValue(50);
    }*/
    m_sliderList_baidu.append(ui->verticalSlider_baidu_01);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_02);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_03);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_04);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_05);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_06);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_07);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_08);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_09);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_10);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_11);
    m_sliderList_baidu.append(ui->verticalSlider_baidu_12);
   /* for(auto slider = m_sliderList_baidu.begin(); slider != m_sliderList_baidu.end(); ++slider)
    {
        (*slider)->setValue(50);
    }*/

	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_01_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_02_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_03_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_04_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_05_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_06_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_07_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_08_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_09_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_10_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_11_m);
	m_sliderList_baidu_m.append(ui->verticalSlider_baidu_12_m);
	//for (auto slider = m_sliderList_baidu_m.begin(); slider != m_sliderList_baidu_m.end(); ++slider)
	//{
	//	(*slider)->setValue(50);
	//}


	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_01_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_02_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_03_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_04_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_05_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_06_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_07_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_08_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_09_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_10_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_11_click);
	m_sliderList_baidu_click.append(ui->verticalSlider_baidu_12_click);
	//for (auto slider = m_sliderList_baidu_m.begin(); slider != m_sliderList_baidu_m.end(); ++slider)
	//{
	//	(*slider)->setValue(50);
	//}







    //m_sliderList_taobao.append(ui->verticalSlider_taobao_01);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_02);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_03);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_04);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_05);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_06);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_07);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_08);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_09);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_10);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_11);
    //m_sliderList_taobao.append(ui->verticalSlider_taobao_12);
   // for(auto slider = m_sliderList_taobao.begin(); slider != m_sliderList_taobao.end(); ++slider)
   // {
    //    (*slider)->setValue(50);

  //  }

	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_01);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_02);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_03);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_04);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_05);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_06);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_07);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_08);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_09);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_10);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_11);
	//m_sliderList_alibaba.append(ui->verticalSlider_alibaba_12);
	//for (auto slider = m_sliderList_alibaba.begin(); slider != m_sliderList_alibaba.end(); ++slider)
	//{
	//	(*slider)->setValue(50);
	//}


	/*m_sliderList_etao.append(ui->verticalSlider_etao_01);
	m_sliderList_etao.append(ui->verticalSlider_etao_02);
	m_sliderList_etao.append(ui->verticalSlider_etao_03);
	m_sliderList_etao.append(ui->verticalSlider_etao_04);
	m_sliderList_etao.append(ui->verticalSlider_etao_05);
	m_sliderList_etao.append(ui->verticalSlider_etao_06);
	m_sliderList_etao.append(ui->verticalSlider_etao_07);
	m_sliderList_etao.append(ui->verticalSlider_etao_08);
	m_sliderList_etao.append(ui->verticalSlider_etao_09);
	m_sliderList_etao.append(ui->verticalSlider_etao_10);
	m_sliderList_etao.append(ui->verticalSlider_etao_11);
	m_sliderList_etao.append(ui->verticalSlider_etao_12);*/
	//for (auto slider = m_sliderList_alibaba.begin(); slider != m_sliderList_alibaba.end(); ++slider)
	//{
	//	(*slider)->setValue(50);
	//}

	//url config
 //   ui->dateEdit_start_day->setDate(QDate::currentDate());
 //   ui->dateEdit_start_day->setMinimumDate(QDate::currentDate());
 //   ui->spinBox_duration_days->setValue(7); 
 //   ui->lineEdit_url->setText("www.ruohuai.com");
	//ui->lineEdit_url->setToolTip("设置你要提高流量的淘宝宝贝地址或者网站");//piikee 20141201 增加tooltips
 //   ui->lineEdit_url->setFocus();
 //   ui->lineEdit_referer->setText("");
	//ui->lineEdit_referer->setToolTip("设置流量从哪里来，比如淘宝可以设置从搜索结果来，\r\n这时可以在淘宝搜索一个词，然后复制搜索结果的url放到这里。");//piikee 20141201 增加tooltips


	//baidu config
	ui->dateEdit_start_day_baidu->setDate(QDate::currentDate());
	ui->dateEdit_start_day_baidu->setMinimumDate(QDate::currentDate());
	ui->spinBox_duration_days_baidu->setValue(7);
    ui->lineEdit_baidu_keyword->setText("piikee");	
	ui->lineEdit_baidu_keyword_b->setText("piikeeseo");

	ui->lineEdit_baidu_keyword->setToolTip("这里输入百度搜索主词,比如要搜索piikee出现相关搜索piikee seo,那么这里输入主词piikee");
	ui->lineEdit_baidu_keyword_b->setToolTip("这里输入百度相关搜索的词的下拉词，比如要搜索piikee出现相关搜索piikee seo，那么这里输入piikee seo就可以。");

    ui->lineEdit_baidu_keyword->setFocus();

	//baidu_m config
	ui->dateEdit_start_day_baidu_m->setDate(QDate::currentDate());
	ui->dateEdit_start_day_baidu_m->setMinimumDate(QDate::currentDate());
	ui->spinBox_duration_days_baidu_m->setValue(7);
	ui->lineEdit_baidu_keyword_m->setText("seo");
	ui->lineEdit_baidu_keyword_b_m->setText("seo piikee");

	ui->lineEdit_baidu_keyword_m->setToolTip("这里输入百度搜索主词,比如要搜索seo出现相关搜索seo piikee,那么这里输入主词piikee");
	ui->lineEdit_baidu_keyword_b_m->setToolTip("这里输入百度相关搜索的词的下拉词或者联想词，比如要搜索seo出现相关搜索seo piikee，那么这里输入seo piikee。");

	ui->lineEdit_baidu_keyword_m->setFocus();
	//baidu_click config
	ui->dateEdit_start_day_baidu_click->setDate(QDate::currentDate());
	ui->dateEdit_start_day_baidu_click->setMinimumDate(QDate::currentDate());
	ui->spinBox_duration_days_baidu_click->setValue(7);
	ui->lineEdit_baidu_keyword_click->setText("piikee");
	ui->lineEdit_baidu_keyword_b_click->setText("萍客小居[piikee的博客]");

	ui->lineEdit_baidu_keyword_click->setToolTip("这里输入百度搜索词,比如要搜索关键词piikee进行点击，那么这里输入piikee");
	ui->lineEdit_baidu_keyword_b_click->setToolTip("这里输入搜索结果要点击的标题包含的词，填整个标题也可以[注意是百度搜索结果显示的你的网站的标题]。");

	ui->lineEdit_baidu_keyword_click->setFocus();
	//taobao config
 //   ui->dateEdit_start_day_3->setDate(QDate::currentDate());
 //   ui->dateEdit_start_day_3->setMinimumDate(QDate::currentDate());
 //   ui->spinBox_duration_days_3->setValue(7);
 //   ui->lineEdit_taobao_keyword->setText("推广软件 萍客电商");
	//ui->lineEdit_taobao_keyword->setToolTip("这里写入你要搜索的主词");//piikee 20141201 增加tooltips
 //   ui->lineEdit_taobao_keyword->setFocus();

	//ui->lineEdit_taobao_keyword_b->setToolTip("这里写入你要让搜索下拉框或者联想区要出现的整个词");//piikee 20141201 增加tooltips
	//ui->lineEdit_taobao_keyword_b->setFocus();

 //   ui->lineEdit_taobao_shop->setText("");
	//ui->lineEdit_taobao_shop->setToolTip("1，这里填入你店铺的旺旺名字，可以让搜索结果进入你店铺\r\n2，如果留空不填那么流量会随机进到搜索结果任何一个产品\r\n3，如果填入一个不存在的旺旺名，那么流量不进任何搜索结果\r\n4，如果填入|，那么流量点进搜索结果右边直通车宝贝");//piikee 20141201 增加tooltips
	//ui->spinBox_day_ip_count_3->setToolTip("词的搜索指数每天搜索量");
	//ui->spinBox_pv_raito_3->setToolTip("这里的PV就是同一个买家搜索进店多少次，一般默认1就可以");

	//alibaba config
	/*ui->dateEdit_start_day_alibaba->setDate(QDate::currentDate());
	ui->dateEdit_start_day_alibaba->setMinimumDate(QDate::currentDate());
	ui->spinBox_duration_days_alibaba->setValue(7);
	ui->lineEdit_alibaba_keyword->setText("不锈钢饭盒");
	ui->lineEdit_alibaba_keyword->setToolTip("这里填入搜索的主词"); 
	ui->lineEdit_alibaba_keyword->setFocus();

	ui->lineEdit_alibaba_keyword_b->setText("不锈钢饭盒米高");
	ui->lineEdit_alibaba_keyword_b->setToolTip("这里填入希望在阿里巴巴搜索主词下拉框出现的整个词");*/

	//etao config
	/*ui->dateEdit_start_day_etao->setDate(QDate::currentDate());
	ui->dateEdit_start_day_etao->setMinimumDate(QDate::currentDate());
	ui->spinBox_duration_days_etao->setValue(7);
	ui->lineEdit_etao_keyword->setText("下拉框萍客电商");
	ui->lineEdit_etao_keyword->setToolTip("这里填入希望在一淘或者淘宝下拉框出现的整个词");
	ui->lineEdit_etao_keyword->setFocus();*/




	//addbutton config
   /* connect(ui->pushButton_add_url, SIGNAL(clicked()), this, SLOT(onCommitTaskUrl()));*/
    connect(ui->pushButton_add_baidu, SIGNAL(clicked()), this, SLOT(onCommitTaskBaidu()));
	connect(ui->pushButton_add_baidu_m, SIGNAL(clicked()), this, SLOT(onCommitTaskBaidu_m()));
	connect(ui->pushButton_add_baidu_click, SIGNAL(clicked()), this, SLOT(onCommitTaskBaidu_click()));
	/*connect(ui->pushButton_add_taobao, SIGNAL(clicked()), this, SLOT(onCommitTaskTaobao()));
	connect(ui->pushButton_add_alibaba, SIGNAL(clicked()), this, SLOT(onCommitTaskAlibaba()));
	connect(ui->pushButton_add_etao, SIGNAL(clicked()), this, SLOT(onCommitTaskEtao()));
*/

	connect(ui->pushButton_vip_query_user, SIGNAL(clicked()), this, SLOT(onVipQueryUser()));
	connect(ui->pushButton_vip_add_score, SIGNAL(clicked()), this, SLOT(onVipAddScore()));
	connect(ui->pushButton_vip_min_score, SIGNAL(clicked()), this, SLOT(onVipMinScore()));

    //准备数据模型
    m_myTasksModel = new QStandardItemModel();
    m_myTasksModel->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("KEY")));
    m_myTasksModel->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("任务类型")));
    m_myTasksModel->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("开始时间")));
    m_myTasksModel->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("结束时间")));
    m_myTasksModel->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("关键字")));
	m_myTasksModel->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("店铺/后缀词")));
	m_myTasksModel->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("每日次数")));
	m_myTasksModel->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("当前次数")));
	m_myTasksModel->setHorizontalHeaderItem(8, new QStandardItem(QObject::tr("历史总次数")));



	//利用setModel()方法将数据模型与QTableView绑定
    ui->tableView_tasks->setModel(m_myTasksModel);
    ui->tableView_tasks->hideColumn(0);
    connect(ui->tableView_tasks, SIGNAL(clicked(QModelIndex)),
            this, SLOT(onMyTasksClicked(QModelIndex)));
    
    //设置表格的各列的宽度值
    ui->tableView_tasks->setColumnWidth(0, 0);
    ui->tableView_tasks->setColumnWidth(1, 80);
    ui->tableView_tasks->setColumnWidth(2, 100);
    ui->tableView_tasks->setColumnWidth(3, 100);
    ui->tableView_tasks->setColumnWidth(4, 100);
	ui->tableView_tasks->setColumnWidth(5, 75);
	ui->tableView_tasks->setColumnWidth(6, 75);
	ui->tableView_tasks->setColumnWidth(7, 75);
	 
	//默认显示行头，如果你觉得不美观的话，我们可以将隐藏
    //ui->tableView_tasks->verticalHeader()->hide();
    //设置选中时为整行选中
    ui->tableView_tasks->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_tasks->setSelectionMode(QAbstractItemView::SingleSelection);
    //设置表格的单元为只读属性，即不能编辑
    ui->tableView_tasks->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //如果你用在QTableView中使用右键菜单，需启用该属性
    //ui->tableView_tasks->setContextMenuPolicy(Qt::CustomContextMenu);

    /*
    task_model->setItem(0, 0, new QStandardItem("流量精灵"));
    task_model->setItem(0, 1, new QStandardItem("141108"));
    task_model->setItem(0, 2, new QStandardItem("141208"));
    task_model->setItem(0, 3, new QStandardItem("piikee.net/hehe/hehe/hehe/loooong/looong/lonng"));
    task_model->setItem(0, 4, new QStandardItem("土木学院"));
    
    //设置单元格文本居中，张三的数据设置为居中显示
    task_model->item(0, 0)->setTextAlignment(Qt::AlignCenter);
    task_model->item(0, 1)->setTextAlignment(Qt::AlignCenter);
    task_model->item(0, 2)->setTextAlignment(Qt::AlignCenter);
    task_model->item(0, 3)->setTextAlignment(Qt::AlignCenter);
    task_model->item(0, 4)->setTextAlignment(Qt::AlignCenter);
    //设置单元格文本颜色，张三的数据设置为红色
    task_model->item(0, 0)->setForeground(QBrush(QColor(255, 0, 0)));
    task_model->item(0, 1)->setForeground(QBrush(QColor(255, 0, 0)));
    task_model->item(0, 2)->setForeground(QBrush(QColor(255, 0, 0)));
    task_model->item(0, 3)->setForeground(QBrush(QColor(255, 0, 0)));
    task_model->item(0, 4)->setForeground(QBrush(QColor(255, 0, 0)));
    //将字体加粗
    task_model->item(0, 0)->setFont( QFont( "Times", 10, QFont::Black ) );
    task_model->item(0, 1)->setFont( QFont( "Times", 10, QFont::Black ) );
    task_model->item(0, 2)->setFont( QFont( "Times", 10, QFont::Black ) );
    task_model->item(0, 3)->setFont( QFont( "Times", 10, QFont::Black ) );
    task_model->item(0, 4)->setFont( QFont( "Times", 10, QFont::Black ) );
    //设置排序方式，按年龄降序显示
    task_model->sort(3, Qt::DescendingOrder);
     */
	ui->tabWidget->setCurrentIndex(0);
	icon.addFile(QStringLiteral(":/icon/ico.png"), QSize(), QIcon::Normal, QIcon::Off);
	trayIcon = new QSystemTrayIcon(this);
	this->trayIcon->setToolTip("萍客电商营销宝");
	this->trayIcon->setIcon(icon);
//	this->trayIcon->setIcon(QIcon("ico.ico"));
    connect(ui->pushButton_stop_task, SIGNAL(clicked()), this, SLOT(onCancelTaskClicked()));
	connect(ui->saveSet, SIGNAL(clicked()), this, SLOT(onSaveButtonClicked()));
	connect(this->trayIcon, SIGNAL( activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onSystemTrayIconClicked(QSystemTrayIcon::ActivationReason)));

	QStringList configstr = readConfig();  
	if (QString(configstr[2].trimmed()) == QString("true").toLatin1().toBase64().trimmed())
		ui->checkBox->setChecked(true);
	else 		 
		ui->checkBox->setChecked(false); 

	if (QString(configstr[3].trimmed()) == QString("true").toLatin1().toBase64())
	 	ui->checkBox_2->setChecked(true);
	 else
	 ui->checkBox_2->setChecked(false); 

}

CuoUserWindow::~CuoUserWindow()
{
    delete ui;
}

//piikee 20141202 新窗口打开广告或公告
void CuoUserWindow::openADsUrl(const QUrl &url)
{
	QString str = url.toString();
 
	QDesktopServices::openUrl(url);
}

void CuoUserWindow::onSaveButtonClicked()
{ 
	   QString  username, password, autologin, autostart;
	   QStringList ql = readConfig();
	   username = ql[0].trimmed();
	   password = ql[1].trimmed();
	   autologin = QString(ui->checkBox->isChecked() ? "true" : "false").toLatin1().toBase64();
	   autostart = QString(ui->checkBox_2->isChecked() ? "true" :"false").toLatin1().toBase64();
	 

	   writeConfig(username + "\r\n" + password + "\r\n" + autologin.trimmed() + "\r\n" + autostart.trimmed());
	   QMessageBox qm;
	   qm.setWindowTitle( "保存设置" );
	   qm.setWindowIcon(icon);
	 //  qm.setMinimumWidth(120);
	   qm.setText("   保存成功    ");
	   qm.exec();

}

void CuoUserWindow::onMyTasksClicked(const QModelIndex &index)
{
    m_selectedItem = m_myTasksModel->itemFromIndex(index);
    m_selectedItem = m_myTasksModel->itemFromIndex(m_myTasksModel->index(index.row(), 0));
}
//void CuoUserWindow::onCommitTaskUrl()
//{
//    commitTaskUrl();
//}
//void CuoUserWindow::onCommitTaskTaobao()
//{
//    commitTaskTaobao();
//}
void CuoUserWindow::onCommitTaskBaidu()
{
    commitTaskBaidu();
}

void CuoUserWindow::onCommitTaskBaidu_m()
{
	commitTaskBaidu_m();
}

void CuoUserWindow::onCommitTaskBaidu_click()
{
	commitTaskBaidu_click();
}
//void CuoUserWindow::onCommitTaskAlibaba()
//{
//	commitTaskAlibaba();
//}
//void CuoUserWindow::onCommitTaskEtao()
//{
//	commitTaskEtao();
//}

void CuoUserWindow::onCancelTaskClicked()
{
    cancelTask();
}
void CuoUserWindow::onVipQueryUser()
{
	vipQueryUser();
}
void CuoUserWindow::onVipAddScore()
{
	vipAddScore();
}
void CuoUserWindow::onVipMinScore()
{
	vipMinScore();
}

void CuoUserWindow::refreshUserInfo()
{
    ui->label_name->setText(CuoUserData::getInstance()->getName());
    ui->label_score->setText(CuoUserData::getInstance()->getScore());
    ui->label_today_score->setText(CuoUserData::getInstance()->getTodayScore());
    ui->label_ip->setText(CuoUserData::getInstance()->getIp());
	if (CuoUserData::getInstance()->getRole() == "BOSS" || CuoUserData::getInstance()->getRole() == "PARTNER")
	{
		if (!ui->tab_partner->isEnabled())
		{
			ui->tabWidget->insertTab(9, ui->tab_partner, "积分充值");
			ui->tab_partner->setEnabled(true);
		}
	}
}
void CuoUserWindow::refreshVipInfo(const QString& msg)
{
	QString name = ui->lineEdit_vip_search_name->text();
	ui->textEdit_vip_log->append(QString("[%1] %2").arg(name).arg(msg));
}
void CuoUserWindow::refreshTaskInfo(const QString& key)
{
    QList<QStandardItem *> items = m_myTasksModel->findItems(key, Qt::MatchExactly, 0);
    CuoTask* t = CuoTaskList::getInstance()->getTask(key);
    if (items.count() > 0)
    {
        int row = (items[0])->row();
        m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 1))->setText(t->getType());
		m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 2))->setText(t->getStartDay());
        m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 3))->setText(t->getEndDay());
        m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 4))->setText(t->getKeyword());  
		m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 5))->setText(t->getKeyword_b()); 
		m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 6))->setText(t->getDayIpCount());
		m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 7))->setText(t->getDayCompletedIpCount());
		m_myTasksModel->itemFromIndex(m_myTasksModel->index(row, 8))->setText(t->getTotalCompletedCount());
	}
    else
    {
        items.push_back(new QStandardItem(t->getKey()));
        items.push_back(new QStandardItem(t->getType()));
        items.push_back(new QStandardItem(t->getStartDay()));
        items.push_back(new QStandardItem(t->getEndDay()));
        items.push_back(new QStandardItem(t->getKeyword()));
		items.push_back(new QStandardItem(t->getKeyword_b()));
		items.push_back(new QStandardItem(t->getDayIpCount()));
		items.push_back(new QStandardItem(t->getDayCompletedIpCount()));
		items.push_back(new QStandardItem(t->getTotalCompletedCount()));
		m_myTasksModel->insertRow(0, (items));
    }
}

//void CuoUserWindow::_commitTaskUrl()
//{
//    QString url         = ui->lineEdit_url->text();
//    QString ua          = "";//ui->lineEdit_ua->text();
//    QString referer     = ui->lineEdit_referer->text();
//    QString start_day   = QString::number(int(ui->dateEdit_start_day->dateTime().toMSecsSinceEpoch()/1000));
//    QString duration_day= ui->spinBox_duration_days->text();
//    QString day_ip_count= ui->spinBox_day_ip_count->text();
//    QString pv_raito= ui->spinBox_pv_raito->text();
//    QString time_distribution;
//    for(auto slider = m_sliderList_url.begin(); slider != m_sliderList_url.end(); ++slider)
//    {
//        time_distribution.append(QString::number((*slider)->value()));
//        time_distribution.append(" ");
//        auto next = slider + 1;
//        if (next != m_sliderList_url.end()) {
//            time_distribution.append(QString::number(((*slider)->value()+(*next)->value())/2));
//            time_distribution.append(" ");
//        }
//        else
//        {
//            time_distribution.append(QString::number(((*slider)->value()+(*(m_sliderList_url.begin()))->value())/2));
//        }
//    }
//    
//    PROTO_NAME(task) taskData;
//    taskData.type               = "url";
//    taskData.start_day           = start_day;
//    taskData.duration_day           = duration_day;
//    taskData.time_distribution   = time_distribution;
//    taskData.day_ip_count           = day_ip_count;
//    taskData.pv_raito           = pv_raito; 
//    taskData.url        = url;
//    taskData.ua         = ua;
//    taskData.referer    = referer;
//    CuoNetIO::getInstance()->send(&taskData);
//    //    emit aboutToLogin();
//    
//}

//void CuoUserWindow::_commitTaskTaobao()
//{
//    QString keyword         = ui->lineEdit_taobao_keyword->text();
//	QString keyword_b = ui->lineEdit_taobao_keyword_b->text();
//
//	QString shop_name = ui->lineEdit_taobao_shop->text().trimmed();//piikee 20141201 删除空格
//    QString start_day       = QString::number(int(ui->dateEdit_start_day_3->dateTime().toMSecsSinceEpoch()/1000));
//    QString duration_day    = ui->spinBox_duration_days_3->text();
//    QString day_ip_count    = ui->spinBox_day_ip_count_3->text();
//    QString pv_raito        = ui->spinBox_pv_raito_3->text();
//    QString time_distribution;
//    for(auto slider = m_sliderList_taobao.begin(); slider != m_sliderList_taobao.end(); ++slider)
//    {
//        time_distribution.append(QString::number((*slider)->value()));
//        time_distribution.append(" ");
//        auto next = slider + 1;
//        if (next != m_sliderList_taobao.end()) {
//            time_distribution.append(QString::number(((*slider)->value()+(*next)->value())/2));
//            time_distribution.append(" ");
//        }
//        else
//        {
//            time_distribution.append(QString::number(((*slider)->value()+(*(m_sliderList_taobao.begin()))->value())/2));
//        }
//    }
//    
//    PROTO_NAME(task) taskData;
//    taskData.type               = "taobao";
//    taskData.start_day          = start_day;
//    taskData.duration_day       = duration_day;
//    taskData.time_distribution  = time_distribution;
//    taskData.day_ip_count       = day_ip_count;
//    taskData.pv_raito           = pv_raito;
//    taskData.keyword            = keyword;
//	taskData.keyword_b = keyword_b;
//
//    taskData.shop               = shop_name;
//    CuoNetIO::getInstance()->send(&taskData);
//}

void CuoUserWindow::_commitTaskBaidu()
{
    QString keyword         = ui->lineEdit_baidu_keyword->text();
	QString keyword_b = ui->lineEdit_baidu_keyword_b->text();

	//QString shop = ui->lineEdit_baidu_shop->text();
    //QString ua          = "";//ui->lineEdit_ua->text();
    //QString referer     = ui->lineEdit_referer->text();
	QString start_day = QString::number(int(ui->dateEdit_start_day_baidu->dateTime().toMSecsSinceEpoch() / 1000));
	QString duration_day = ui->spinBox_duration_days_baidu->text();
	QString day_ip_count = ui->spinBox_day_ip_count_baidu->text();
    QString pv_raito= ui->spinBox_pv_raito_2->text();
    QString time_distribution;
    for(auto slider = m_sliderList_baidu.begin(); slider != m_sliderList_baidu.end(); ++slider)
    {
        time_distribution.append(QString::number((*slider)->value()));
        time_distribution.append(" ");
        auto next = slider + 1;
        if (next != m_sliderList_baidu.end()) {
            time_distribution.append(QString::number(((*slider)->value()+(*next)->value())/2));
            time_distribution.append(" ");
        }
        else
        {
            time_distribution.append(QString::number(((*slider)->value()+(*(m_sliderList_baidu.begin()))->value())/2));
        }
    }
    
    PROTO_NAME(task) taskData;
    taskData.type               = "baidu";
    taskData.start_day           = start_day;
    taskData.duration_day           = duration_day;
    taskData.time_distribution   = time_distribution;
    taskData.day_ip_count           = day_ip_count;
    taskData.pv_raito           = pv_raito;
    taskData.keyword        = keyword;
	taskData.keyword_b = keyword_b;

//	taskData.shop = shop; 
    CuoNetIO::getInstance()->send(&taskData);
}
void CuoUserWindow::commitTaskBaidu()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("提交任务");
	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = 指数* 天数*20 】").arg(ui->spinBox_day_ip_count_baidu->text().toInt()* ui->spinBox_duration_days_baidu->text().toInt()*20));//【所需积分 = ((ip * 20 + (pv - 1)*ip * 4)*天数】"

	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	msgBox.setButtonText(QMessageBox::Save, "确定");
	msgBox.setButtonText(QMessageBox::Cancel, "取消");
	int ret = msgBox.exec();
	switch (ret) {
	case QMessageBox::Save:
		_commitTaskBaidu();
		break;
	case QMessageBox::Cancel:
		// Cancel was clicked
		break;
	default:
		// should never be reached
		break;
	}
}



void CuoUserWindow::_commitTaskBaidu_m()
{
	QString keyword = ui->lineEdit_baidu_keyword_m->text();
	QString keyword_b = ui->lineEdit_baidu_keyword_b_m->text();

	//QString shop = ui->lineEdit_baidu_shop->text();
	//QString ua          = "";//ui->lineEdit_ua->text();
	//QString referer     = ui->lineEdit_referer->text();
	QString start_day = QString::number(int(ui->dateEdit_start_day_baidu_m->dateTime().toMSecsSinceEpoch() / 1000));
	QString duration_day = ui->spinBox_duration_days_baidu_m->text();
	QString day_ip_count = ui->spinBox_day_ip_count_baidu_m->text();
	QString pv_raito = ui->spinBox_pv_raito_2->text();
	QString time_distribution;
	for (auto slider = m_sliderList_baidu_m.begin(); slider != m_sliderList_baidu_m.end(); ++slider)
	{
		time_distribution.append(QString::number((*slider)->value()));
		time_distribution.append(" ");
		auto next = slider + 1;
		if (next != m_sliderList_baidu_m.end()) {
			time_distribution.append(QString::number(((*slider)->value() + (*next)->value()) / 2));
			time_distribution.append(" ");
		}
		else
		{
			time_distribution.append(QString::number(((*slider)->value() + (*(m_sliderList_baidu_m.begin()))->value()) / 2));
		}
	}

	PROTO_NAME(task) taskData;
	taskData.type = "baidu_m";
	taskData.start_day = start_day;
	taskData.duration_day = duration_day;
	taskData.time_distribution = time_distribution;
	taskData.day_ip_count = day_ip_count;
	taskData.pv_raito = pv_raito;
	taskData.keyword = keyword;
	taskData.keyword_b = keyword_b;

	//	taskData.shop = shop; 
	CuoNetIO::getInstance()->send(&taskData);
}
void CuoUserWindow::commitTaskBaidu_m()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("提交任务");
	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = 指数* 天数*20 】").arg(ui->spinBox_day_ip_count_baidu_m->text().toInt()* ui->spinBox_duration_days_baidu_m->text().toInt() * 20));//【所需积分 = ((ip * 20 + (pv - 1)*ip * 4)*天数】"

	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	msgBox.setButtonText(QMessageBox::Save, "确定");
	msgBox.setButtonText(QMessageBox::Cancel, "取消");
	int ret = msgBox.exec();
	switch (ret) {
	case QMessageBox::Save:
		_commitTaskBaidu_m();
		break;
	case QMessageBox::Cancel:
		// Cancel was clicked
		break;
	default:
		// should never be reached
		break;
	}
}

void CuoUserWindow::_commitTaskBaidu_click()
{
	QString keyword = ui->lineEdit_baidu_keyword_click->text();
	QString keyword_b = ui->lineEdit_baidu_keyword_b_click->text();

	//QString shop = ui->lineEdit_baidu_shop->text();
	//QString ua          = "";//ui->lineEdit_ua->text();
	//QString referer     = ui->lineEdit_referer->text();
	QString start_day = QString::number(int(ui->dateEdit_start_day_baidu_click->dateTime().toMSecsSinceEpoch() / 1000));
	QString duration_day = ui->spinBox_duration_days_baidu_click->text();
	QString day_ip_count = ui->spinBox_day_ip_count_baidu_click->text();
	QString pv_raito = ui->spinBox_pv_raito_2->text();
	QString time_distribution;
	for (auto slider = m_sliderList_baidu_click.begin(); slider != m_sliderList_baidu_click.end(); ++slider)
	{
		time_distribution.append(QString::number((*slider)->value()));
		time_distribution.append(" ");
		auto next = slider + 1;
		if (next != m_sliderList_baidu_click.end()) {
			time_distribution.append(QString::number(((*slider)->value() + (*next)->value()) / 2));
			time_distribution.append(" ");
		}
		else
		{
			time_distribution.append(QString::number(((*slider)->value() + (*(m_sliderList_baidu_click.begin()))->value()) / 2));
		}
	}

	PROTO_NAME(task) taskData;
	taskData.type = "baidu_click";
	taskData.start_day = start_day;
	taskData.duration_day = duration_day;
	taskData.time_distribution = time_distribution;
	taskData.day_ip_count = day_ip_count;
	taskData.pv_raito = pv_raito;
	taskData.keyword = keyword;
	taskData.keyword_b = keyword_b;

	//	taskData.shop = shop; 
	CuoNetIO::getInstance()->send(&taskData);
}
void CuoUserWindow::commitTaskBaidu_click()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("提交任务");
	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = 指数* 天数*20 】").arg(ui->spinBox_day_ip_count_baidu_click->text().toInt()* ui->spinBox_duration_days_baidu_click->text().toInt() * 20));//【所需积分 = ((ip * 20 + (pv - 1)*ip * 4)*天数】"

	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	msgBox.setButtonText(QMessageBox::Save, "确定");
	msgBox.setButtonText(QMessageBox::Cancel, "取消");
	int ret = msgBox.exec();
	switch (ret) {
	case QMessageBox::Save:
		_commitTaskBaidu_click();
		break;
	case QMessageBox::Cancel:
		// Cancel was clicked
		break;
	default:
		// should never be reached
		break;
	}
}



//void CuoUserWindow::_commitTaskAlibaba()
//{
//	QString keyword = ui->lineEdit_alibaba_keyword->text();
//	QString keyword_b = ui->lineEdit_alibaba_keyword_b->text();
//
//	QString shop = QString("12");
//	//QString ua          = "";//ui->lineEdit_ua->text();
//	//QString referer     = ui->lineEdit_referer->text();
//	QString start_day = QString::number(int(ui->dateEdit_start_day_alibaba->dateTime().toMSecsSinceEpoch() / 1000));
//	QString duration_day = ui->spinBox_duration_days_alibaba->text();
//	QString day_ip_count = ui->spinBox_day_ip_count_alibaba->text();
//	QString pv_raito = ui->spinBox_pv_raito_2->text();
//	QString time_distribution;
//	for (auto slider = m_sliderList_alibaba.begin(); slider != m_sliderList_alibaba.end(); ++slider)
//	{
//		time_distribution.append(QString::number((*slider)->value()));
//		time_distribution.append(" ");
//		auto next = slider + 1;
//		if (next != m_sliderList_alibaba.end()) {
//			time_distribution.append(QString::number(((*slider)->value() + (*next)->value()) / 2));
//			time_distribution.append(" ");
//		}
//		else
//		{
//			time_distribution.append(QString::number(((*slider)->value() + (*(m_sliderList_alibaba.begin()))->value()) / 2));
//		}
//	}
//
//	PROTO_NAME(task) taskData;
//	taskData.type = "alibaba";
//	taskData.start_day = start_day;
//	taskData.duration_day = duration_day;
//	taskData.time_distribution = time_distribution;
//	taskData.day_ip_count = day_ip_count;
//	taskData.pv_raito = pv_raito;
//	taskData.keyword = keyword;
//	taskData.keyword_b = keyword_b;
//
//	taskData.shop = shop;
//	CuoNetIO::getInstance()->send(&taskData);
//}
//
//void CuoUserWindow::_commitTaskEtao()
//{
//	QString keyword = ui->lineEdit_etao_keyword->text();
//	QString shop = QString("12");
//	//QString ua          = "";//ui->lineEdit_ua->text();
//	//QString referer     = ui->lineEdit_referer->text();
//	QString start_day = QString::number(int(ui->dateEdit_start_day_etao->dateTime().toMSecsSinceEpoch() / 1000));
//	QString duration_day = ui->spinBox_duration_days_etao->text();
//	QString day_ip_count = ui->spinBox_day_ip_count_etao->text();
//	QString pv_raito = ui->spinBox_pv_raito_2->text();
//	QString time_distribution;
//	for (auto slider = m_sliderList_etao.begin(); slider != m_sliderList_etao.end(); ++slider)
//	{
//		time_distribution.append(QString::number((*slider)->value()));
//		time_distribution.append(" ");
//		auto next = slider + 1;
//		if (next != m_sliderList_etao.end()) {
//			time_distribution.append(QString::number(((*slider)->value() + (*next)->value()) / 2));
//			time_distribution.append(" ");
//		}
//		else
//		{
//			time_distribution.append(QString::number(((*slider)->value() + (*(m_sliderList_etao.begin()))->value()) / 2));
//		}
//	}
//
//	PROTO_NAME(task) taskData;
//	taskData.type = "etao";
//	taskData.start_day = start_day;
//	taskData.duration_day = duration_day;
//	taskData.time_distribution = time_distribution;
//	taskData.day_ip_count = day_ip_count;
//	taskData.pv_raito = pv_raito;
//	taskData.keyword = keyword;
//	taskData.shop = shop;
//	CuoNetIO::getInstance()->send(&taskData);
//}
//
//void CuoUserWindow::commitTaskUrl()
//{
//	QMessageBox msgBox;
//	msgBox.setWindowTitle("提交任务");
//	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = ((ip * 10 + (pv - 1)*ip * 2)*天数】").arg((10 * ui->spinBox_day_ip_count->text().toInt() + (ui->spinBox_pv_raito->text().toInt() - 1)* ui->spinBox_day_ip_count->text().toInt() * 2)* ui->spinBox_duration_days->text().toInt()));
//	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
//	msgBox.setDefaultButton(QMessageBox::Save);
//	msgBox.setButtonText(QMessageBox::Save, "确定");
//	msgBox.setButtonText(QMessageBox::Cancel, "取消");
//	int ret = msgBox.exec();
//	switch (ret) {
//	case QMessageBox::Save:
//		_commitTaskUrl();
//		break;
//	case QMessageBox::Cancel:
//		// Cancel was clicked
//		break;
//	default:
//		// should never be reached
//		break;
//	}
//}
//
//void CuoUserWindow::commitTaskTaobao()
//{
//    QMessageBox msgBox;
//	msgBox.setWindowTitle("提交任务");
//  //  msgBox.setText("提交任务");
//	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = 指数*天数* 20】").arg((20 * ui->spinBox_day_ip_count_3->text().toInt() + (ui->spinBox_pv_raito_3->text().toInt() - 1)* ui->spinBox_day_ip_count_3->text().toInt() * 4)* ui->spinBox_duration_days_3->text().toInt()));//【所需积分 = ((ip * 20 + (pv - 1)*ip * 4)*天数】"
//    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
//    msgBox.setDefaultButton(QMessageBox::Save);
//    msgBox.setButtonText(QMessageBox::Save, "确定");
//    msgBox.setButtonText(QMessageBox::Cancel, "取消");
//    int ret = msgBox.exec();
//    switch (ret) {
//        case QMessageBox::Save:
//            _commitTaskTaobao();
//            break;
//        case QMessageBox::Cancel:
//            // Cancel was clicked
//            break;
//        default:
//            // should never be reached
//            break;
//    }
//}

//void CuoUserWindow::commitTaskAlibaba()
//{
//	QMessageBox msgBox;
//	msgBox.setWindowTitle("提交任务");
//	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = 指数* 天数 * 20 】").arg((20 * ui->spinBox_day_ip_count_alibaba->text().toInt() + (ui->spinBox_pv_raito_2->text().toInt() - 1)* ui->spinBox_day_ip_count_alibaba->text().toInt() * 4)* ui->spinBox_duration_days_alibaba->text().toInt()));//【所需积分 = ((ip * 20 + (pv - 1)*ip * 4)*天数】"
//
//	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
//	msgBox.setDefaultButton(QMessageBox::Save);
//	msgBox.setButtonText(QMessageBox::Save, "确定");
//	msgBox.setButtonText(QMessageBox::Cancel, "取消");
//	int ret = msgBox.exec();
//	switch (ret) {
//	case QMessageBox::Save:
//		_commitTaskAlibaba();
//		break;
//	case QMessageBox::Cancel:
//		// Cancel was clicked
//		break;
//	default:
//		// should never be reached
//		break;
//	}
//}


//void CuoUserWindow::commitTaskEtao()
//{
//	QMessageBox msgBox;
//	msgBox.setWindowTitle("提交任务");
//	msgBox.setInformativeText(QString("确定提交任务吗？该任务需要%1积分。\r\n【所需积分 = 指数* 天数 * 20 】").arg((20 * ui->spinBox_day_ip_count_etao->text().toInt() + (ui->spinBox_pv_raito_2->text().toInt() - 1)* ui->spinBox_day_ip_count_etao->text().toInt() * 4)* ui->spinBox_duration_days_etao->text().toInt()));//【所需积分 = ((ip * 20 + (pv - 1)*ip * 4)*天数】"
//
//	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
//	msgBox.setDefaultButton(QMessageBox::Save);
//	msgBox.setButtonText(QMessageBox::Save, "确定");
//	msgBox.setButtonText(QMessageBox::Cancel, "取消");
//	int ret = msgBox.exec();
//	switch (ret) {
//	case QMessageBox::Save:
//		_commitTaskEtao();
//		break;
//	case QMessageBox::Cancel:
//		// Cancel was clicked
//		break;
//	default:
//		// should never be reached
//		break;
//	}
//}

void CuoUserWindow::_cancelTask()
{
    PROTO_NAME(cancel) taskData;
    m_selectedItem = m_myTasksModel->itemFromIndex(m_myTasksModel->index(m_selectedItem->row(), 0));
    taskData.key = m_selectedItem->text();
    CuoNetIO::getInstance()->send(&taskData);
    m_selectedItem = m_myTasksModel->itemFromIndex(m_myTasksModel->index(m_selectedItem->row(), 5));
    m_selectedItem->setText("已停止");
    m_selectedItem->setForeground(QBrush(QColor(255, 0, 0)));
}

void CuoUserWindow::cancelTask()
{
    if(m_selectedItem==NULL)
        return;
    QMessageBox msgBox;
	msgBox.setWindowTitle("删除任务");
    msgBox.setInformativeText("确定停止选中的任务吗？停止后系统将返回剩余天数的积分。");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    msgBox.setButtonText(QMessageBox::Save, "确定");
    msgBox.setButtonText(QMessageBox::Cancel, "取消");
    int ret = msgBox.exec();
    switch (ret) {
        case QMessageBox::Save:
            _cancelTask();
            break;
        case QMessageBox::Cancel:
            // Cancel was clicked
            break;
        default:
            // should never be reached
            break;
    }
}
void CuoUserWindow::vipQueryUser()
{
	QString name = ui->lineEdit_vip_search_name->text();
	if (name == "")
	{
		QMessageBox qm;
		qm.setWindowTitle("错误");
		qm.setWindowIcon(icon);
		//  qm.setMinimumWidth(120);
		qm.setText("   请输入正确的名称    ");
		qm.exec();
		return;
	}
	PROTO_NAME(queryuser) queryData;
	queryData.name = name;
	CuoNetIO::getInstance()->send(&queryData);
}
void CuoUserWindow::vipAddScore()
{
	QString name = ui->lineEdit_vip_search_name->text();
	if (name == "")
	{
		QMessageBox qm;
		qm.setWindowTitle("错误");
		qm.setWindowIcon(icon);
		//  qm.setMinimumWidth(120);
		qm.setText("   请输入正确的名称    ");
		qm.exec();
		return;
	}
	QString score = ui->spinBox_add_min_score->text();
	PROTO_NAME(addscore) addscoreData;
	addscoreData.name = name;
	addscoreData.score = score;
	CuoNetIO::getInstance()->send(&addscoreData);
}
void CuoUserWindow::vipMinScore()
{
	QString name = ui->lineEdit_vip_search_name->text();
	if (name == "")
	{
		QMessageBox qm;
		qm.setWindowTitle("错误");
		qm.setWindowIcon(icon);
		//  qm.setMinimumWidth(120);
		qm.setText("   请输入正确的名称    ");
		qm.exec();
		return;
	}
	QString score = ui->spinBox_add_min_score->text();
	PROTO_NAME(addscore) addscoreData;
	addscoreData.name = name;
	addscoreData.score = QString("-%1").arg(score);
	CuoNetIO::getInstance()->send(&addscoreData);
}


void CuoUserWindow::writeConfig(QString info)
{
	QFile file(QApplication::applicationDirPath() + "/core_config.ini");
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{ 
		QTextStream txtOutput(&file); 
		txtOutput << info << endl; 
		file.close();
	}
	else  ;
}

QStringList CuoUserWindow::readConfig()
{
	QStringList lines; 
	if (QFile::exists(QApplication::applicationDirPath() + "/core_config.ini"))
	{

		QFile file(QApplication::applicationDirPath() + "/core_config.ini");
		if (file.open(QIODevice::ReadOnly)) {
			QTextStream stream(&file);
			QString line = stream.readLine();

			while (!line.isNull()) {
				lines.append(line);
				line = stream.readLine();

			}
			file.close();
		}

	}
	else
		 ;

	return lines;

}

  

void CuoUserWindow::changeEvent(QEvent *e)
{
	
	if (e->WindowStateChange)
	{
		switch (this->windowState())
		{
		
		case Qt::WindowMinimized:
		{
									
			 this->trayIcon->show();
			 this->trayIcon->showMessage("", "我在这，双击还原哦", QSystemTrayIcon::MessageIcon::Information, 800);
			
		  this->hide();
			 this->setWindowState(Qt::WindowMinimized);
			 //e->ignore();
			 break;
		}
	 
		default: break;
		}
	}
} 

void CuoUserWindow::onSystemTrayIconClicked(QSystemTrayIcon::ActivationReason reason)
{
	 
	switch (reason)
	{
	 
	case QSystemTrayIcon::DoubleClick:
		//恢复窗口显示	
		this->show();
	  this->setWindowState(Qt::WindowNoState);
	 // this->setWindowState(Qt::WindowActive);
	
		break;
	default:
		break;
	}

}

