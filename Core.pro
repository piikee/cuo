#-------------------------------------------------
#
# Project created by QtCreator 2014-07-12T20:07:00
#
#-------------------------------------------------

QT       += core gui webkitwidgets network

DEFINES  +=

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Core
TEMPLATE = app


INCLUDEPATH += "./include/"
#LIBS += -L"./lib/" -lprotobuf
LIBS += -stdlib=libc++

QMAKE_CXXFLAGS += -stdlib=libc++
QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS += -mmacosx-version-min=10.7 #1
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.7 #2

SOURCES += main.cpp\
    cuowebview.cpp \
    cuonetio.cpp \
    cuoengine.cpp \
    xor.cpp \
    cuowindowmanager.cpp \
    cuoapplication.cpp \
    cuouserwindow.cpp \
    cuologinwindow.cpp \
    cuoregisterwindow.cpp \
    cuoproto.cpp \
    cuopower.cpp \
    cuolabor.cpp \
    cuouser.cpp  \
    cuotask.cpp \   
    disclaimer.cpp

HEADERS  += \
    cuowebview.h \
    cuonetio.h \
    cuoengine.h \
    xor.h \
    cuowindowmanager.h \
    cuoapplication.h \
    cuouserwindow.h \
    cuologinwindow.h \
    cuoregisterwindow.h \
    cuoproto.h \
    cuopower.h \
    cuolabor.h \
    cuouser.h   \
    cuotask.h \
    disclaimer.h

FORMS    += \
    cuoregisterwindow.ui \
    cuouserwindow.ui \
    cuologinwindow.ui \
    disclaimer.ui

RESOURCES = jquery.qrc \
    images.qrc
RC_FILE = Core.rc