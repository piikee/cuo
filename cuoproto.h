﻿#ifndef PROTO_H
#define PROTO_H
#include <QString>


#define PROTO_NAME(name) PROTO_##name
#define PROTO_NAME_POINTER(name) PROTO_##name* 

#define DEFINE_PROTO_BEGIN(name)                                \
    class PROTO_NAME(name) : public IProto                      \
    {                                                           \
        public:                                                 \
            const char* getProtoName();                         \
            IProto* clone();                                    \
            int serialize(QString& output);                     \
            int unserialize(const QString& input);              \

#define DEFINE_PROTO_FIELD(name, type)                          \
            type name;

#define DEFINE_PROTO_END                                        \
    };

typedef struct PROTO_FILED_TYPE{
    const char * n;
    unsigned long p;
}PROTO_FILED_TYPE;
#define OFFSETOF(type, field)    ((unsigned long) &(((type *) 0)->field))

#define DEFINE_PROTO_IMP_BEGIN(name)                            \
    PROTO_FILED_TYPE PROTO_##name##_FIELDS[] = {                \

#define DEFINE_PROTO_FIELD_IMP(name, field)                     \
        { #field, OFFSETOF(PROTO_##name, field) },              \

#define DEFINE_PROTO_IMP_END(name)                              \
        { NULL, NULL}                                           \
    };                                                          \
    IProto* PROTO_NAME(name)::clone()                           \
    {                                                           \
        return new PROTO_NAME(name);                            \
    }                                                           \
                                                                \
    const char* PROTO_NAME(name)::getProtoName()                \
    {                                                           \
        return #name;                                           \
    }                                                           \
                                                                \
    int PROTO_NAME(name)::serialize(QString &output)            \
    {                                                           \
        QJsonDocument json;                                     \
        QJsonObject jsonObject;                                 \
        PROTO_FILED_TYPE* f = &( PROTO_##name##_FIELDS [0]) ;   \
        for(; f->n != NULL; ++f)                                \
        {                                                       \
            jsonObject[f->n] = *(QString*)((unsigned long)this + f->p); \
        }                                                       \
        json.setObject(jsonObject);                             \
        output = json.toJson(QJsonDocument::Compact);           \
        return 0;                                               \
    }                                                           \
    int PROTO_NAME(name)::unserialize(const QString &input)     \
    {                                                           \
        QJsonDocument json = QJsonDocument::fromJson(QString(input).toUtf8());\
        QJsonObject jsonObject = json.object();                 \
        PROTO_FILED_TYPE* f = &( PROTO_##name##_FIELDS [0]) ;   \
        for(; f->n != NULL; ++f)                                \
        {                                                       \
            *(QString*)((unsigned long)this + f->p) = jsonObject[f->n].toString();\
        }                                                       \
        return 0;                                               \
    }

class IProto
{
public:
    //void onProto(IProto*);
    virtual IProto* clone() = 0;
    virtual const char* getProtoName()=0;
    virtual int serialize(QString& output);
    virtual int unserialize(const QString& input);
    virtual ~IProto(){};
    //virtual void emitSig(){emit onProto(this);}
};

typedef void (*PROTOHANDLE)(IProto *);


DEFINE_PROTO_BEGIN(login)
DEFINE_PROTO_FIELD(name, QString)
DEFINE_PROTO_FIELD(password, QString)
DEFINE_PROTO_END


DEFINE_PROTO_BEGIN(logout)
DEFINE_PROTO_FIELD(title, QString)
DEFINE_PROTO_FIELD(msg, QString)
DEFINE_PROTO_END


DEFINE_PROTO_BEGIN(register)
DEFINE_PROTO_FIELD(name, QString)
DEFINE_PROTO_FIELD(group, QString)
DEFINE_PROTO_FIELD(password, QString)
DEFINE_PROTO_FIELD(qq, QString)
DEFINE_PROTO_FIELD(email, QString)
DEFINE_PROTO_FIELD(tel, QString)
DEFINE_PROTO_END


DEFINE_PROTO_BEGIN(heartbeat)
DEFINE_PROTO_END


DEFINE_PROTO_BEGIN(unlock)
DEFINE_PROTO_END


DEFINE_PROTO_BEGIN(message)
DEFINE_PROTO_FIELD(title, QString)
DEFINE_PROTO_FIELD(msg, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(user)
DEFINE_PROTO_FIELD(name, QString)
DEFINE_PROTO_FIELD(ip, QString)
DEFINE_PROTO_FIELD(score, QString)
DEFINE_PROTO_FIELD(role, QString)
DEFINE_PROTO_FIELD(today_score, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(labor)
DEFINE_PROTO_FIELD(content, QString)
DEFINE_PROTO_FIELD(key, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(report)
DEFINE_PROTO_FIELD(key, QString)
DEFINE_PROTO_FIELD(proxy_ip, QString)
DEFINE_PROTO_FIELD(result, QString)
DEFINE_PROTO_FIELD(success, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(task)
DEFINE_PROTO_FIELD(type, QString)
DEFINE_PROTO_FIELD(start_day, QString)
DEFINE_PROTO_FIELD(duration_day, QString)
DEFINE_PROTO_FIELD(time_distribution, QString)
DEFINE_PROTO_FIELD(day_ip_count, QString)
DEFINE_PROTO_FIELD(pv_raito, QString)
DEFINE_PROTO_FIELD(url, QString)
DEFINE_PROTO_FIELD(ua, QString)
DEFINE_PROTO_FIELD(referer, QString)
DEFINE_PROTO_FIELD(keyword, QString)
DEFINE_PROTO_FIELD(keyword_b, QString)
DEFINE_PROTO_FIELD(shop, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(mytask)
DEFINE_PROTO_FIELD(key, QString)
DEFINE_PROTO_FIELD(type, QString)
DEFINE_PROTO_FIELD(start_day, QString)
DEFINE_PROTO_FIELD(end_day, QString)
DEFINE_PROTO_FIELD(keyword, QString)
DEFINE_PROTO_FIELD(keyword_b, QString)
DEFINE_PROTO_FIELD(shop, QString)
DEFINE_PROTO_FIELD(day_ip_count, QString)
DEFINE_PROTO_FIELD(total_completed_count, QString)
DEFINE_PROTO_FIELD(day_completed_ip_count, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(cancel)
DEFINE_PROTO_FIELD(key, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(queryuser)
DEFINE_PROTO_FIELD(name, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(queryresult)
DEFINE_PROTO_FIELD(score, QString)
DEFINE_PROTO_FIELD(error, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(addscore)
DEFINE_PROTO_FIELD(name, QString)
DEFINE_PROTO_FIELD(score, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(iamproxy)
DEFINE_PROTO_FIELD(ip, QString)
DEFINE_PROTO_END

DEFINE_PROTO_BEGIN(expireproxy)
DEFINE_PROTO_END


#endif // PROTO_H
