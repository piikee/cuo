﻿#include "cuonetio.h"

#include "xor.h"
#include <QTimer>
#include <QTime>
#include <QCryptographicHash>
#include <QNetworkProxy>
#include "cuoproto.h"
#include "cuoapplication.h"

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

static EndPoint* g_endpoint = NULL;

void setEndPoint(QString host, quint16 port, QByteArray key)
{
    if (NULL == g_endpoint) {
        g_endpoint = new EndPoint;
    }
    g_endpoint->host = host;
    g_endpoint->port = port;
    g_endpoint->key  = key;
}
EndPoint* getEndPoint()
{
    return g_endpoint;
}

//-------------------------------------------------------
CuoNetIO* CuoNetIO::pInstance = NULL;

void onProtoHeartbeat(IProto* p)
{
	Q_UNUSED(p)
	CuoNetIO::getInstance()->heartbeat();
}

CuoNetIO* CuoNetIO::getInstance()
{
    if(NULL == pInstance)
    {
        pInstance = new CuoNetIO();
        if(!pInstance->isInited())
        {
            pInstance->init();
        }
    }
    return pInstance;
}

void CuoNetIO::Release()
{
    if(NULL != pInstance)
    {
        delete pInstance;
        pInstance = NULL;
    }
}

bool CuoNetIO::isInited() const
{
    return m_inited;
}

void CuoNetIO::init()
{
    m_inited = true;
    m_host  = getEndPoint()->host;
    m_port  = getEndPoint()->port;
    m_key   = getEndPoint()->key;
    
    m_reconnectTimer = new QTimer(this);
    connect(m_reconnectTimer, SIGNAL(timeout()), this, SLOT(_connect()));
    m_keepaliveTimer = new QTimer(this);
    connect(m_keepaliveTimer, SIGNAL(timeout()), this, SLOT(_keepalive()));
	m_heartbeatTimer = new QTimer(this);
	connect(m_heartbeatTimer, SIGNAL(timeout()), this, SLOT(_heartbeatTimeout()));
	_createTcpSocket();

	IProto* p_heartbeat = new PROTO_NAME(heartbeat);
	this->registerProto(p_heartbeat, onProtoHeartbeat);
}

CuoNetIO::CuoNetIO()
: m_tcpSocket(NULL)
, m_tryconnect(0)
, m_reconnectTimer(NULL)
, m_keepaliveTimer(NULL)
, m_heartbeatTimer(NULL)
, m_inited(false)
{
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
}

CuoNetIO::~CuoNetIO()
{
}

void CuoNetIO::_createTcpSocket()
{
    if (m_tcpSocket) {
        return;
    }
    m_tcpSocket = new QTcpSocket(this);
    m_tcpSocket->setProxy(QNetworkProxy::NoProxy);
    connect(m_tcpSocket, SIGNAL(connected()), this, SLOT(_connected()));
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(_receive()));
    connect(m_tcpSocket, SIGNAL(disconnected()), this, SLOT(_disconnected()));
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(_socketError(QAbstractSocket::SocketError)));
}

void CuoNetIO::_connect()
{
    _createTcpSocket();
    if(m_tcpSocket->state() != QAbstractSocket::UnconnectedState)
        return;
    m_blockSize = 0;
    m_tcpSocket->abort();
    //key = QString(KEY).toLocal8Bit();
    m_session = 0;
    ++m_tryconnect;
    m_reconnectTimer->stop();
    cuolog()<<"Connecting";
    m_tcpSocket->connectToHost(m_host, m_port);
}

void CuoNetIO::start()
{
    _connect();
}
void CuoNetIO::restart()
{
	cuolog() << "Restart!!!";
	_disconnect();
	start();
}
void CuoNetIO::_connected()
{
    m_tryconnect = 0;
    m_reconnectTimer->stop();
	if (!cuoApp()->isSlaver())
	{
		m_keepaliveTimer->start(1000);
		m_heartbeatTimer->start(60 * 1000);
	}
    cuolog()<<"connected";
    emit sigConnected();
}

void CuoNetIO::_disconnected()
{
    // QMap<quint16, QSharedPointer<CuoRequest> > r;
    // requests.swap(r);
    m_keepaliveTimer->stop();
	m_keepaliveTimer->stop();
    cuolog()<<"disconnected";
    emit sigDisconnected();
}

void CuoNetIO::_disconnect()
{
    m_tryconnect = 100;
    m_tcpSocket->abort();
}

void CuoNetIO::rawSend(const QString& send)
{
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setByteOrder(QDataStream::BigEndian);
	out.setVersion(QDataStream::Qt_4_0);
	out << (quint16)0;
	QString response = Xor::convertData(m_key, send.toUtf8()).toBase64();
	QByteArray rep = response.toUtf8();//.toLocal8Bit();
	cuolog() << "send data: " << send;
	//cuolog()<<"xor data: "<<rep;
	out.writeRawData(rep.data(), rep.size());
	out.device()->seek(0);
	out << (quint16)(block.size() - sizeof(quint16));
	m_tcpSocket->write(block);
}

void CuoNetIO::send(IProto* proto)
{
    if(m_tcpSocket->state() != QAbstractSocket::ConnectedState)
        return;
    ++m_session;
    
    QString data;
    proto->serialize(data);
    QString send = QString("{\"command\":\"%1\",\"data\":%2}").arg(proto->getProtoName()).arg(data);

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setByteOrder(QDataStream::BigEndian);
    out.setVersion(QDataStream::Qt_4_0);
    out << (quint16)0;
    QString response = Xor::convertData(m_key, send.toUtf8()).toBase64();
	QByteArray rep = response.toUtf8();//.toLocal8Bit();
    cuolog()<<"send data: "<<send;
    //cuolog()<<"xor data: "<<rep;
    out.writeRawData(rep.data(), rep.size());
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));
    m_tcpSocket->write(block);
}

void CuoNetIO::_receive()
{
    QDataStream in(m_tcpSocket);

    do
    {
        if (m_blockSize == 0) {
            if (m_tcpSocket->bytesAvailable() < (int)sizeof(quint16))
                return;
            in >> m_blockSize;
        }

        if (m_tcpSocket->bytesAvailable() < m_blockSize)
            return;

        QByteArray buffer(m_blockSize, Qt::Uninitialized);
        in.readRawData(buffer.data(), m_blockSize);
        QByteArray response = QByteArray::fromBase64(buffer);
        response = Xor::convertData(m_key, response);
        cuolog()<<"receive data: "<<response;
        onData(response);
        m_blockSize = 0;
    } while (true);
}

void CuoNetIO::onData(QByteArray& data)
{
    QJsonDocument json = QJsonDocument::fromJson(data);
    //QJsonDocument json = QJsonDocument::fromJson(QString(data).toUtf8());
    if(!json.isObject())
    {
        emit sigDataError(data);
        return;
    }
    QJsonObject o = json.object();
    if(!o["command"].isString())
    {
        emit sigDataError(data);
        return;
    }
    QString command = o["command"].toString();
    json.setObject(o["data"].toObject());                             \
    triggerProto(command, json.toJson(QJsonDocument::Compact));
}

void CuoNetIO::registerProto(IProto* p, PROTOHANDLE fun)
{
    m_protos[p->getProtoName()] = QPair<IProto*, PROTOHANDLE>(p, fun);
}

void CuoNetIO::triggerProto(const QString& name, const QString& data)
{
    if(m_protos.contains(name))
    {
        IProto* p = m_protos[name].first->clone();
        p->unserialize(data);
        m_protos[name].second(p);
        delete p;
    }
    else
    {
        cuolog()<<"Error Command: "<<name;
    }
}

void CuoNetIO::change_key(QJsonDocument data)
{
    m_key.clear();
    m_key = QByteArray::fromBase64(m_key.append(data.object()["data"].toString()));
    return;
}

void CuoNetIO::_tryReconnect()
{
    //if(m_tryconnect == 50)
    //    return;
    int msec = 1000 * (m_tryconnect+1) > 5*60*1000 ? 5*60*1000 : 1000 * (m_tryconnect+1);
    msec = (qrand() % msec) + 2000;
    cuolog()<<"reconnect in "<<msec<<" msec";
    m_reconnectTimer->start(msec);
}

void CuoNetIO::_keepalive()
{
    m_keepaliveTimer->stop();
    PROTO_NAME(heartbeat) heartbeat;
    this->send(&heartbeat);
    m_keepaliveTimer->start(10000);
}
void CuoNetIO::_heartbeatTimeout()
{
	restart();
}
void CuoNetIO::heartbeat()
{
	m_heartbeatTimer->start(60*1000);
}
void CuoNetIO::_socketError(QAbstractSocket::SocketError socketError)
{
    cuolog()<<"socketError: "<<socketError;
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        _tryReconnect();
        break;
    case QAbstractSocket::HostNotFoundError:
        _tryReconnect();
        break;
    case QAbstractSocket::ConnectionRefusedError:
        _tryReconnect();
        break;
    default:
        _tryReconnect();
    }
}
