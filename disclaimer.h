#ifndef DISCLAIMER_H
#define DISCLAIMER_H

#include <QWidget>

namespace Ui {
class disclaimer;
}

class disclaimer : public QWidget
{
    Q_OBJECT

public:
    explicit disclaimer(QWidget *parent = 0);
    ~disclaimer();
	
signals: 

	public slots :
		void closebutton();
private:
    Ui::disclaimer *ui;
};

#endif // DISCLAIMER_H
