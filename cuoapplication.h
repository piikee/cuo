﻿#ifndef CUOAPPLICATION_H
#define CUOAPPLICATION_H

#include <QApplication>
#include <QTcpSocket>

class CuoWindowManager;
class CuoNetIO;
class CuoPower;
class QTcpServer;
class QProcess;
class QTcpSocket;
class IProto;
class QAbstractSocket;

class CuoApplication : public QApplication
{

    Q_OBJECT

public:
    CuoApplication( int argc, char *argv[] );
    ~CuoApplication();

    bool start();

    void setCurrentUserUid( qint32 uid );
	void EnableLog();

	void Master();
	void Slaver();
	void sendSlaver(IProto*);
	bool isSlaver();
	void restartSlaver();
	
    //bool isAccountLogined( const QString &accountName );
    //bool isAccountStartLogin(const QString &accountName);
    //void makeAccountLogined(const QString &accountName);
    //void makeAccountStartLogin(const QString &accountName);

    //void postYYIPCMessage( UINT32 message, UINT32 wParam, UINT32 lParam );

    //void toggleMainframe();

    //void peekMessage();

    CuoWindowManager *  windowManager() const;
    CuoNetIO *          netIO();

    //bool needRestart() const{return m_needRestart;}
    //int getSessionToken() {return m_sessionToken;}
    //bool isRestarted() {return m_isRelogin;}
    //void doRestart();
    //void doRestart(int nReason);

    //void addCrashProperty( const QString &key, const QString &value );

protected slots:
	void acceptConnection();
	void slaverDisconnected();
	void masterDisconnected();
	void receiveSlaverData();
	void localConnectionError(QAbstractSocket::SocketError e);
	void slaverRestareTimeout();
	void engineStart();
	//void on_messageEvent( UINT32 messageId, UINT32 wParam, UINT32 lParam );
    //void on_hotkeyEvent(int hotkeyId);
    //void on_copyDataEvent( QByteArray &copyData, UINT32 dataId );
    //void on_getUserInfo( UINT32 callbackWindowId, bool &result );
    //void on_getUserInfo2( UINT32 callbackWindowId, int& result ); // for pip

    //void on_getUserInfo3(UINT32 callbackWindowId);	//for activite
    //void on_getCookie(UINT32 callbackWindowId);	//for activete
    //void on_requestOpenID(UINT32 callbackWindowId, PCOPYDATASTRUCT pCopyData);
    //void on_requestLoginSilent(UINT32 callBackWindow,PCOPYDATASTRUCT buf,LRESULT& result);
    //void on_gotoOpenIDWeb();

    //void on_getUDBTicket(UINT32 callbackWindowId, UINT32 serviceid);//for activex
    //void on_application_abouttoquit();

protected:
    //virtual bool event( QEvent *e );
    //virtual bool winEventFilter(MSG *message, long *result);

private:
    //void on_readyPoll();
    //void on_logouted(PASSPORT_ERROR_CODE, IPropBagPtrCR);

private:
    //void _processArguments();
    //void _initEnv();
    //void _initTranslator();
    //bool _initBiz();
    //void _initResourcePath();
    //void _initComponentServices();

    //bool _addYYInstance( bool isJump );
    //void _initBizParams();

    //void _executeAsyncTasks();
    //bool _initUIResource();
    //void _processReportData(const QString &var);

private:

private:

    QString					m_currentVersionYYAppPath;
    QString					m_currentYYUserAppPath;
    qint32					m_mutexInstance;
    CuoWindowManager*       m_windowManager;
    CuoNetIO*               m_netIO;
    CuoPower*               m_power;

	bool					m_isSlaver;
	QProcess*				m_slaverProcess;
	QTcpServer*				m_tcpServer;
	QTcpSocket*				m_tcpServerConnection;
	QTcpSocket*				m_tcpClient;
	quint16					m_port;
	quint16					m_receiveSlaverBlockSize;

	QTimer *                m_slaverRestareTimer;
	QTimer *                m_slaverEngineTimer;
	bool					m_isEnableProxy;
};

CuoApplication *cuoApp();
#endif // CUOAPPLICATION_H
