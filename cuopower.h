﻿//
//  cuouser.h
//  Core
//
//  Created by Yiwei Wang on 11/2/14.
//
//

#ifndef __Core__cuopower__
#define __Core__cuopower__

#include <QObject>
#include <QNetworkProxy>
#include <qnetworkreply.h>
#include <functional>
class CuoEngine;
class CuoLabor;
class QTimer;
//------------------------------------------------

class QNetworkAccessManager;

class CuoPower : public QObject
{
    Q_OBJECT
    
public:
    static CuoPower* getInstance();
    
    static void Release();
    
    virtual void init();
    
    virtual bool isInited() const;
    
    void EnableProxy(bool);
	void RefreshProxy(bool is_next_proxy_ip);
	void ShowWebView(bool);
    void onLabor(const QString& key, const QString& content);

private:
	void _trigger();
	void _request();
	void _parse(const QByteArray&);

public slots:
	void reply();
	void replyError(QNetworkReply::NetworkError code);
	void _retryTimeout();

private:
    explicit CuoPower();
    virtual ~CuoPower();
    
    
signals:
    
private slots:
    
    
private slots:

private:
    static CuoPower* pInstance;
    bool m_inited;
    
    CuoEngine*      m_engine;
    
    bool m_isEnableProxy;

	QString m_key;
	QString m_content;

	QNetworkAccessManager* m_netManager;

	int m_proxyUsed;
	QList<QNetworkProxy> m_proxyListInUse;

	int m_tryRequestProxyCount;
	QTimer* m_proxyRetryTimer;
};

//------------------------------------------------


#endif /* defined(__Core__cuopower__) */
