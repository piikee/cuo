﻿#include <QString>
#include <time.h>
#include "xor.h"

QByteArray Xor::convertData(QByteArray key, QByteArray text)
{
    if (key.size() == 0)
    {
        return text;
    }
    else
    {
        QByteArray convertedText(text.size(), 0);
        for (int i = 0; i < text.size(); i++)
        {
            uchar a = text[i];
            uchar b = key[i%key.size()];
            uchar c = a ^ b;
            convertedText[i] = c;
        }
        return convertedText;
    }
}
