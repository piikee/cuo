﻿#include "QtWidgets"
#include "CuoRegisterWindow.h"
#include "ui_CuoRegisterWindow.h"
#include "disclaimer.h"
#include "ui_disclaimer.h"
#include "cuonetio.h"


#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif


CuoRegisterWindow::CuoRegisterWindow(QWidget *parent)
:QWidget(parent),
    ui(new Ui::CuoRegisterWindow)
{
    ui->setupUi(this);
	ui->pushButton_3->setFlat(true); 
	//setWindowIcon(QIcon("ico.ico"));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(_register()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(_onLoginButtonClick()));
	connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(labeldisClick()));
}
 

CuoRegisterWindow::~CuoRegisterWindow()
{
    delete ui;
}

void CuoRegisterWindow::labeldisClick()
{
	disclaimer *diswin = new disclaimer();
	diswin->setWindowModality(Qt::ApplicationModal);
	diswin->show();


}

void CuoRegisterWindow::_register()
{
	 
    QString name = ui->lineEdit->text();
    QString password = ui->lineEdit_2->text();
    QString password2 = ui->lineEdit_3->text();
    QString qq = ui->lineEdit_4->text();
    QString email = ui->lineEdit_5->text();
    QString tel = ui->lineEdit_6->text();
	
    if (name.size()<3||name.size()>10)
    {
        return displayError("注册失败", "你名字长度要在3-10范围");
    }
    if (password.size()<6||password.size()>20)
    {
        return displayError("注册失败", "密码6位以上18位以内");
    }
    if (password != password2)
    {
        return displayError("注册失败", "密码不相同");
    }
    if (qq.size()<4||qq.size()>15)
    {
        return displayError("注册失败", "请正确输入您的QQ号码");
    }
    if (email.size()<4||email.size()>30)
    {
        return displayError("注册失败", "请正确输入您的电子邮箱");
    }
    if (tel.size()<4||tel.size()>18)
    {
        return displayError("注册失败", "请正确输入您的电话号码");
    }
	if (!ui->checkBox->isChecked())
	{
		labeldisClick(); 
		return displayError("注册失败", "请先阅读并同意《免责声明》后勾选同意再注册！"  );
	}
    PROTO_NAME(register) registerData;
    registerData.name = name;
	registerData.group = "GHOST";  //该用户群组，必须是英文或数字，不要包含符号或特殊字符
    registerData.password = password.toLatin1().toBase64();
    registerData.qq = qq;
    registerData.email = email;
    registerData.tel = tel;
    CuoNetIO::getInstance()->send(&registerData);
    this->setEnabled(false);
	emit aboutToRegister();
 
}

void CuoRegisterWindow::_onLoginButtonClick()
{
    this->hide();
    emit aboutToLogin();
}


void CuoRegisterWindow::displayError(QString title, QString msg)
{
    QMessageBox::information(this, title, msg);
}
