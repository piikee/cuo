﻿#ifndef CUOENGINE_H
#define CUOENGINE_H

#include <QWidget>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QJsonArray>
#include <QJsonDocument>
#include <QNetworkProxy>
#include "cuolabor.h"

class QTimer;
class CuoWebView;
class CuoWebPage;

#define enginelog()     qDebug()<<"[Engine] "

class CuoJavaScriptBridge : public QObject
{
    Q_OBJECT
public:
    CuoJavaScriptBridge();
    Q_INVOKABLE void sentences(QString command, QString state, QString scripttime);
    
    
signals:
    void sigCommand(QString command, QString state, QString scripttime);
    
private:
    
};


class CuoEngine : public QWidget
{
    Q_OBJECT
public:
    static CuoEngine* getInstance();
    
    static void Release();
    
    virtual void init();
    
    virtual bool isInited() const;
    

    typedef enum {OFF, ON} State;
    
    State getState() const;
    
private:
    explicit CuoEngine();
    virtual ~CuoEngine();

signals:
    void sigFinish(QString command, QString data);

public slots:

    void Start();
    void Stop();
    void SetProxy(const QNetworkProxy& proxy);
	void ShowWebView(bool);
    void newLabor(const QString&, const QString&);

private:
    int _createWebview();
    int _createBridge();
    int _createInitTimer();
    int _createStepTimer();
    int _createTotalTimer();
    int _createScriptTimer();

	void _adjustScreen();
	bool _isOutOfDesktop(QWidget *w);


    void _start();
    void _step();
    void _process(ILaborStep*);
    void _processURL(LaborURL*);
    void _processJAVASCRIPT(LaborJAVASCRIPT*);
    void _complete(const QString& success, const QString& result);

private slots:
    void _initTimeout();
    void _stepTimeout();
    void _totalTimeout();
    
    void _urlLoadStarted();
    void _urlLoadCompleted(bool);
	void _webViewInputCompleted();
    void _javaScriptWindowObjectCleared();
    void _onJavaScriptCall(QString, QString, QString);
    void _javaScriptCall();
    
    void _onScriptButtonClicked();

private:
    bool                    m_inited;
    State                   m_state;
    CuoWebView *            m_webview;
	QWidget*				m_webviewWidget;
	bool					m_jQueryInstalled;
    CuoJavaScriptBridge *   m_javascriptBridge;
    QPlainTextEdit*         m_scriptTextEdit;
    QPushButton*            m_scriptPushButton;
    QTimer *                m_initTimer;
    QTimer *                m_stepTimer;
    QTimer *                m_totalTimer;
    
    QTimer *                m_scriptTimer;
    bool                    m_scriptWaitting;
    QString                 m_scriptCommand;
    QStringList             m_scriptState;

    QString                 m_jQuery;

    int                     m_step;
	int						m_stepcount;
	int						m_inputStepCount;
	int						m_ignoreCompletedEvent;
	CuoLabor*               m_labor;
    
    QString                 m_proxyIP;

    static CuoEngine*       pInstance;


};

#endif // CUOENGINE_H
