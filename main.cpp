﻿#include "CuoApplication.h"
#include "cuowindowmanager.h"


#include <windows.h>
#include <rtcapi.h>
int exception_handler(LPEXCEPTION_POINTERS p)
{
	printf("Exception detected during the unit tests!\n");
	exit(1);
}
int runtime_check_handler(int errorType, const char *filename, int linenumber, const char *moduleName, const char *format, ...)
{
	printf("Error type %d at %s line %d in %s", errorType, filename, linenumber, moduleName);
	exit(1);
}


int main(int argc, char *argv[])
{
	DWORD dwMode = SetErrorMode(SEM_NOGPFAULTERRORBOX);
	SetErrorMode(dwMode | SEM_NOGPFAULTERRORBOX);
	SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)&exception_handler);
	_RTC_SetErrorFunc(&runtime_check_handler);


    CuoApplication app(argc, argv); 
    int returnCode = 0;
	 
    if(app.start())
    { 
        returnCode = app.exec();
    }
    return 0;
}
