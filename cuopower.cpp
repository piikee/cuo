﻿//
//  cuouser.cpp
//  Core
//
//  Created by Yiwei Wang on 11/2/14.
//
//

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkProxy> 
#include <qtimer.h>
#include "cuopower.h"
#include "cuoengine.h"
#include "cuonetio.h"
#include "cuolabor.h"
#include <QRegExpValidator>

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif


void onProtoLabor(IProto* p)
{
	auto m = dynamic_cast< PROTO_NAME_POINTER(labor) >(p);
	if (m != NULL) {
		if (CuoPower::getInstance()->isInited()) {
			CuoPower::getInstance()->onLabor(m->key, m->content);
		}
	}
}
void onProtoExpireProxy(IProto* p)
{
	auto m = dynamic_cast< PROTO_NAME_POINTER(expireproxy) >(p);
	if (m != NULL) {
		if (CuoPower::getInstance()->isInited()) {
			CuoPower::getInstance()->RefreshProxy(true);
		}
	}
}

//------------------------------------------------
CuoPower* CuoPower::pInstance = NULL;

CuoPower* CuoPower::getInstance()
{
    if(NULL == pInstance)
    {
        pInstance = new CuoPower();
        if(!pInstance->isInited())
        {
            pInstance->init();
        }
    }
    return pInstance;
}

void CuoPower::Release()
{
    if(NULL != pInstance)
    {
        delete pInstance;
        pInstance = NULL;
    }
}

bool CuoPower::isInited() const
{
    return m_inited;
}

void CuoPower::init()
{
    m_inited = true;
    m_engine = CuoEngine::getInstance();
    m_engine->init();

	m_netManager = new QNetworkAccessManager;

    CuoNetIO* net = CuoNetIO::getInstance();
	IProto* p_labor = new PROTO_NAME(labor);
	net->registerProto(p_labor, onProtoLabor);
	IProto* p_expireproxy = new PROTO_NAME(expireproxy);
	net->registerProto(p_expireproxy, onProtoExpireProxy);
	m_proxyRetryTimer = new QTimer();
	connect(m_proxyRetryTimer, SIGNAL(timeout()), this, SLOT(_retryTimeout()));
}

CuoPower::CuoPower()
: m_inited(false)
, m_isEnableProxy(false)
, m_proxyUsed(0)
, m_tryRequestProxyCount(0)
, m_proxyRetryTimer(NULL)
{
}


CuoPower::~CuoPower()
{
}

void CuoPower::EnableProxy(bool v)
{
    m_isEnableProxy = v;
	RefreshProxy(true);
}
void CuoPower::RefreshProxy(bool is_next_proxy_ip)
{
	if (m_isEnableProxy) {
		if (!is_next_proxy_ip)
		{
			QNetworkProxy p = m_proxyListInUse[m_proxyUsed-1];
			m_engine->SetProxy(p);
			PROTO_NAME(iamproxy) iamproxyData;
			iamproxyData.ip = p.hostName();
			CuoNetIO::getInstance()->send(&iamproxyData);
		}
		else if (m_proxyUsed < m_proxyListInUse.length()) {
			QNetworkProxy p = m_proxyListInUse[m_proxyUsed++];
			m_engine->SetProxy(p);
			PROTO_NAME(iamproxy) iamproxyData;
			iamproxyData.ip = p.hostName();
			CuoNetIO::getInstance()->send(&iamproxyData);
		}
		else
		{
			m_engine->SetProxy(QNetworkProxy::NoProxy);
			return _request();
		}
	}
}
void CuoPower::ShowWebView(bool v)
{
	m_engine->ShowWebView(v);
}

void CuoPower::onLabor(const QString& key, const QString& content)
{
	m_key = key;
	m_content = content;
	_trigger();
}


void CuoPower::_trigger()
{
	m_engine->newLabor(m_key, m_content);
	m_engine->Start();
}

void CuoPower::_request()
{
	m_tryRequestProxyCount++;
	m_proxyListInUse.clear();
	m_proxyUsed = 0;
	if (m_tryRequestProxyCount > 3 && 0)
	{
		m_proxyListInUse.append(QNetworkProxy::NoProxy);
		_trigger();
		return;
	}
	QUrl url("http://c.piikee.net:8080/get");
    QNetworkRequest req(url);
    
    QNetworkReply *reply = m_netManager->get(req);
    connect(reply, SIGNAL(finished()), this, SLOT(reply()));
	connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(replyError(QNetworkReply::NetworkError)));
}

void CuoPower::reply()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            //read data from reply
            QByteArray data = reply->readAll();
            _parse(data);
        } else {
            //get http status code
            int httpStatus = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
			qDebug() << "Error Request Proxy IP, Try again" << httpStatus << m_tryRequestProxyCount;
			//_request();
			m_proxyRetryTimer->start(3000);
			//do some error management
        }
        reply->deleteLater();
    }
}
void CuoPower::replyError(QNetworkReply::NetworkError code)
{
	qDebug() << " replyError  Error Request Proxy IP, Try again" << m_tryRequestProxyCount;
	m_proxyRetryTimer->start(3000);
	//_request();
}
void CuoPower::_retryTimeout()
{
	qDebug() << "_retryTimeout";
	m_proxyRetryTimer->stop();
	_request();
}
void CuoPower::_parse(const QByteArray& data)
{
    QString str = data;
    QStringList l = str.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
	//QStringList m_iplist;//piikee 20141203 IP重复判断
	//bool isin = false;//piikee 20141203 IP重复判断
	if (l.begin() == l.end())
	{
		m_proxyRetryTimer->start(3000);
		return;
	}
    for (auto it = l.begin(); it != l.end(); ++it) {
	//	isin = false;
        QStringList ll = it->split(QRegExp("[:,]"));
		QRegExp ValidIpAddressRegex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
		QRegExpValidator ValIPAddr(ValidIpAddressRegex, 0);
		int pos = 0;
		qDebug() << ll[0] << ll[1];
		if (ValIPAddr.validate(ll[0], pos) == QValidator::Invalid)
		{
			continue;
		}
		 	//piikee 20141203 IP重复判断
	//	for each (QString  var in m_iplist)
		//{
			
		//	if (var == ll[0])
		//	{
			 
			//	isin = true;
			//	break;
			//}
	//	}
		//m_iplist.append(ll[0]);//piikee 20141203 IP重复判断 
	//	if (!isin)//piikee 20141203 IP重复判断
		//{
			QNetworkProxy proxy;
			proxy.setType(QNetworkProxy::HttpProxy);
			proxy.setHostName(ll[0]);
			proxy.setPort(ll[1].toInt());
			m_proxyListInUse.append(proxy);
		//}
    }
	RefreshProxy(true);
}

