﻿#ifndef CUONETIO_H
#define CUONETIO_H

#include <QObject>
#include <QTcpSocket>
#include <QSharedPointer>
#include <QJsonDocument>
#include <QJsonObject>

#include "cuoproto.h"

#define DECLAREPROTO

#define cuolog()     qDebug()<<"[NetIO] "

class CuoRequest;
class QTimer;
class IProto;

typedef struct EndPoint
{
    QString host;
    quint16 port;
    QByteArray key;
}EndPoint;

void setEndPoint(QString host, quint16 port, QByteArray key);
EndPoint* getEndPoint();

class CuoNetIO : public QObject
{
    Q_OBJECT
public:
    static CuoNetIO* getInstance();
    
    static void Release();
    
    virtual void init();
    
    virtual bool isInited() const;

    void start();
	void restart();
    void send(IProto* data);
	void rawSend(const QString&);
    void registerProto(IProto* p, PROTOHANDLE fun);
    void triggerProto(const QString& name, const QString& data);
    void onData(QByteArray&);

	void heartbeat();

private:
    explicit CuoNetIO();
    // QSharedPointer<CuoRequest> cuoSend(QJsonDocument data);
    virtual ~CuoNetIO();


signals:
    void sigConnected();
    void sigDisconnected();
    void sigDataError(QByteArray&);

private slots:

    void change_key(QJsonDocument);

private slots:
    void _createTcpSocket();
    void _connect();
    void _connected();
    void _disconnected();
    void _disconnect();
    void _keepalive();
	void _heartbeatTimeout();
    void _tryReconnect();
    void _socketError(QAbstractSocket::SocketError);
    void _receive();
private:
    QString     m_host;
    quint16     m_port;
    QTcpSocket* m_tcpSocket;
    quint16     m_blockSize;
    quint16     m_session;
    quint16     m_tryconnect;
    QTimer *    m_reconnectTimer;
    QTimer *    m_keepaliveTimer;
	QTimer *	m_heartbeatTimer;
    QByteArray  m_key;
    QMap <QString, QPair<IProto*, PROTOHANDLE> > m_protos;

    static CuoNetIO* pInstance;
    bool m_inited;
};

#endif // CUONETIO_H
