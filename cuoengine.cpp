﻿#include "cuoengine.h"
#include "cuowebview.h"
#include "cuolabor.h"

#include <QtWidgets>
#include <QRegExp>


#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

CuoJavaScriptBridge::CuoJavaScriptBridge()
{
    
}
void CuoJavaScriptBridge::sentences(QString command, QString state, QString scripttime)
{
    emit sigCommand(command, state, scripttime);
}
//////-----------------------
CuoEngine* CuoEngine::pInstance = NULL;

CuoEngine* CuoEngine::getInstance()
{
    if(NULL == pInstance)
    {
        pInstance = new CuoEngine();
        if(!pInstance->isInited())
        {
            pInstance->init();
        }
    }
    return pInstance;
}

void CuoEngine::Release()
{
    if(NULL != pInstance)
    {
        delete pInstance;
        pInstance = NULL;
    }
}

bool CuoEngine::isInited() const
{
    return m_inited;
}
void CuoEngine::init()
{
    if (m_inited) {
        return;
    }
    m_inited = true;
    _createWebview();
    _createBridge();
    _createInitTimer();
    _createStepTimer();
    _createTotalTimer();
    _createScriptTimer();
}


CuoEngine::CuoEngine()
    : m_inited(false)
    , m_state(OFF)
    , m_webview(NULL)
	, m_jQueryInstalled(false)
    , m_javascriptBridge(NULL)
    , m_scriptTextEdit(NULL)
    , m_scriptPushButton(NULL)
    , m_initTimer(NULL)
    , m_stepTimer(NULL)
    , m_totalTimer(NULL)
    , m_scriptTimer(NULL)
    , m_scriptWaitting(false)
    , m_step(0)
	, m_stepcount(0)
    , m_labor(NULL)
{
    QFile file;
    file.setFileName(":/jquery.min.js");
    file.open(QIODevice::ReadOnly);
    m_jQuery = file.readAll();
    m_jQuery.append("\nvar qt = { 'jQuery': jQuery.noConflict(true) };");
    file.close();
}

CuoEngine::~CuoEngine()
{}

CuoEngine::State CuoEngine::getState() const
{
    return m_state;
}

void CuoEngine::ShowWebView(bool v)
{
	if (!m_inited)
        return;
    if (v)
		m_webviewWidget->show();
	else
		m_webviewWidget->hide();
}


bool CuoEngine::_isOutOfDesktop(QWidget *w)
{
	QDesktopWidget *desktopWidget = QApplication::desktop();
	QRect screenRect = desktopWidget->screenGeometry(w);
	QPoint topLeft = w->mapToGlobal(w->geometry().topLeft());
	QPoint bottomRight = w->mapToGlobal(w->geometry().bottomRight());

	if (!screenRect.contains(topLeft) || !screenRect.contains(bottomRight))
		return true;

	return false;
}

void CuoEngine::_adjustScreen()
{
	if (_isOutOfDesktop(m_webviewWidget))
	{
		QDesktopWidget *d = QApplication::desktop();
		int w = d->width();   // returns screen width
		int h = d->height();  // returns screen height

		int random_width = qrand() % (1133 - 1024) + 1024;
		m_webviewWidget->resize((random_width>w)?w:random_width, 800); //taobao的网页没有横向滚动条如果页面宽度太小又不能滚动到右边会导致点击不到

		QSize s = m_webviewWidget->sizeHint();
		QRect r = m_webviewWidget->geometry();
		int mw = s.width();
		int mh = s.height();
		int cw = (w / 2) - (mw / 2);
		int ch = (h / 2) - (mh / 2);
		m_webviewWidget->move(cw, ch);
	}
}
int CuoEngine::_createWebview()
{
    if(m_webview)
    {
        return 0;
    }
    m_webview = new CuoWebView();
	
    m_scriptTextEdit = new QPlainTextEdit();
    m_scriptPushButton = new QPushButton();
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(m_webview, 0, 0);
    mainLayout->addWidget(m_scriptTextEdit, 1, 0);
    mainLayout->addWidget(m_scriptPushButton, 2, 0);
    
    setLayout(mainLayout);
	m_webviewWidget = new QWidget();
	m_webviewWidget->setLayout(mainLayout);
	//m_webviewWidget->hide();
	QDesktopWidget *d = QApplication::desktop();
	int w = d->width();   // returns screen width
	int h = d->height();  // returns screen height

	int random_width = qrand() % (1133 - 1024) + 1024;
	m_webviewWidget->resize((random_width>w) ? w : random_width, 800); //taobao的网页没有横向滚动条如果页面宽度太小又不能滚动到右边会导致点击不到

	_adjustScreen();
    //connect(view, SIGNAL(loadFinished(bool)), SLOT(loadFinished()));
    //connect(view, SIGNAL(titleChanged(QString)), SLOT(adjustTitle()));
    //connect(m_webview, SIGNAL(loadProgress(int)), SLOT(setProgress(int)));
    connect(m_webview, SIGNAL(loadStarted()), SLOT(_urlLoadStarted()));
	connect(m_webview, SIGNAL(loadFinished(bool)), SLOT(_urlLoadCompleted(bool)));
	connect(m_webview, SIGNAL(sigInputCompleted()), SLOT(_webViewInputCompleted()));
	connect(m_webview->page()->mainFrame(), SIGNAL(javaScriptWindowObjectCleared()), SLOT(_javaScriptWindowObjectCleared()));
    
    connect(m_scriptPushButton, SIGNAL(clicked()), SLOT(_onScriptButtonClicked()));
    return 1;
}

int CuoEngine::_createBridge()
{
    if (m_javascriptBridge) {
        return 0;
    }
    m_javascriptBridge = new CuoJavaScriptBridge();
    connect(m_javascriptBridge, SIGNAL(sigCommand(QString, QString, QString)),SLOT(_onJavaScriptCall(QString, QString, QString)));
    return 1;
}

int CuoEngine::_createInitTimer()
{
    if(m_initTimer)
    {
        return 0;
    }
    m_initTimer = new QTimer();
    connect(m_initTimer, SIGNAL(timeout()), this, SLOT(_initTimeout()));
    return 1;
}


int CuoEngine::_createStepTimer()
{
    if(m_stepTimer)
    {
        return 0;
    }
    m_stepTimer = new QTimer();
    connect(m_stepTimer, SIGNAL(timeout()), this, SLOT(_stepTimeout()));
    return 1;
}

int CuoEngine::_createTotalTimer()
{
    if(m_totalTimer)
    {
        return 0;
    }
    m_totalTimer = new QTimer();
    connect(m_totalTimer, SIGNAL(timeout()), this, SLOT(_totalTimeout()));
    return 1;
}

int CuoEngine::_createScriptTimer()
{
    if(m_scriptTimer)
    {
        return 0;
    }
    m_scriptTimer = new QTimer();
    connect(m_scriptTimer, SIGNAL(timeout()), this, SLOT(_javaScriptCall()));
    return 1;
}

void CuoEngine::_initTimeout()
{
    enginelog()<<"Init Timeout!";
    m_initTimer->stop();
    _step();
}

void CuoEngine::_stepTimeout()
{
	enginelog() << "Step Timeout!";
	m_stepTimer->stop();
	if (m_stepcount >= 0)
	{
		m_step += m_stepcount;
		_step();
	}
	else
	{
		_complete("0", QString("%1(%2)").arg("Step Timeout").arg(m_step));
	}
}

void CuoEngine::_totalTimeout()
{
	enginelog() << "Total Timeout!";
	m_totalTimer->stop();
	_complete("0", QString("%1(%2)").arg("Total Timeout").arg(m_step));
}

void CuoEngine::SetProxy(const QNetworkProxy& proxy)
{
    m_proxyIP = proxy.hostName();
	//m_webviewWidget->setWindowTitle(QString("Proxy Model(%1)").arg( m_proxyIP));
    m_webview->setProxy(proxy);
}

void CuoEngine::Start()
{
    if(m_state==OFF)
    {
        m_state = ON;
        _start();
    }
	 m_webviewWidget->setWindowTitle(QString("Working, Proxy(%1)").arg(m_proxyIP));//.arg(m_webview->page()->mainFrame()->toHtml())
}

void CuoEngine::Stop()
{
    _complete("0", "Engine Stop");
}

void CuoEngine::newLabor(const QString& key, const QString& content)
{
    if (m_state==OFF) {
        m_labor = new CuoLabor(key, content);
    }
}

void CuoEngine::_start()
{
    m_step = 0; 
    int inittime = m_labor->getInittime();
    m_initTimer->start(1000*inittime);
    int totaltime = m_labor->getTotaltime();
    m_totalTimer->start(1000*totaltime);
    enginelog()<<"Starting...init time: "<<inittime<<", total time: "<<totaltime;
}

void CuoEngine::_complete(const QString& success, const QString& result)
{
    if(m_state==ON)
    {
        m_stepTimer->stop();
        m_totalTimer->stop();
        m_scriptTimer->stop();
        m_labor->Report(success, result, m_proxyIP);
        if (m_labor) {
            delete m_labor;
        }
        m_state = OFF;
        m_scriptWaitting=false;
    }
 	//QNetworkRequest request(QUrl("about:blank"));
 	//m_webview->load(request);
	
	m_webviewWidget->setWindowTitle(QString("No Working"));
	//m_webviewWidget->setWindowTitle(QString("CleanCookie"));
	m_webview->cleanCookie();//清理cookies

	//m_webviewWidget->setWindowTitle(QString("CleanCookie OK"));
    enginelog()<<"Completed with success "<<success<<" result "<<result<<"...";

    //emit sigFinish();
}

void CuoEngine::_step()
{
    if(m_state!=ON)
        return;
    if(m_step >= m_labor->getStepCount())
    {
        enginelog()<<"Over Steps";
        _complete("0", "Over Steps");
        return;
    }
    m_scriptWaitting = false;
	m_webviewWidget->setWindowTitle(QString("[%2]Working, Proxy(%1)").arg(m_proxyIP).arg(m_webview->page()->mainFrame()->title()));//
	m_webview->stopInput();
    ILaborStep* labor = m_labor->getStep(m_step);
    int steptime = labor->getSteptime();
	m_stepcount = labor->getStepcount();
	m_ignoreCompletedEvent = labor->getIgnoreCompletedEvent();
	enginelog() << "Steping...step: " << m_step << ", step time: " << steptime << ", ignoreCompletedEvent: " << m_ignoreCompletedEvent;
    m_stepTimer->start(1000*steptime);
    //++m_step;
    _process(labor);
}

void CuoEngine::_process(ILaborStep* labor)
{
    QString type = labor->getType();
    if(type == "URL")
    {
        LaborURL* l = dynamic_cast<LaborURL*>(labor);
        return _processURL(l);
    }
    if(type == "JAVASCRIPT")
    {
        LaborJAVASCRIPT* l = dynamic_cast<LaborJAVASCRIPT*>(labor);
        return _processJAVASCRIPT(l);
    }
}

void CuoEngine::_processURL(LaborURL* labor)
{
    enginelog()<<"Processing URL: "<<labor->Url;
    QString defaultUA("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36");
    QString ua = labor->Ua;
    m_webview->setUserAgent(ua);
    QUrl url = labor->Url;
    QNetworkRequest request(url);
    for(auto it = labor->Header.begin(); it != labor->Header.end(); ++it)
    {

        request.setRawHeader( it.key().toUtf8() , it.value().toUtf8());
    }
    //request.setRawHeader("Referer", "http://facebook.com");
    m_webview->load(request);
}

void CuoEngine::_processJAVASCRIPT(LaborJAVASCRIPT* labor)
{
    enginelog()<<"Processing Javascript: "<<labor->Script;
    QString script = labor->Script;
	if (!m_jQueryInstalled)
	{
		m_webview->page()->mainFrame()->evaluateJavaScript(m_jQuery);
		enginelog() << "evaluateJavaScript jQuery";
		m_jQueryInstalled = true;
	}
    m_webview->page()->mainFrame()->evaluateJavaScript(script);
}
void CuoEngine::_urlLoadStarted()
{
	 
    enginelog()<<"Url Load Started!";
}
void CuoEngine::_urlLoadCompleted(bool completed)
{
	if (m_ignoreCompletedEvent){
		return;
	}
    if (completed) {
        enginelog()<<"Url Load Completed!";
        m_webview->page()->mainFrame()->evaluateJavaScript(m_jQuery);
		m_jQueryInstalled = true;
        m_stepTimer->stop();
        ++m_step;
        _step();
    }
    else{
        enginelog()<<"Url Load Error!";
    }
}
void CuoEngine::_webViewInputCompleted()
{
	enginelog() << "WebView Input Completed!";
	m_stepTimer->stop();
	if (m_inputStepCount > 0)
	{
		m_step += m_inputStepCount;
		_step();
	}
}

void CuoEngine::_javaScriptWindowObjectCleared()
{
    enginelog()<<"JavaScript Window Object Cleared!";
    m_webview->page()->mainFrame()->addToJavaScriptWindowObject("cuobridge", m_javascriptBridge);
	m_jQueryInstalled = false;
    //m_webview->page()->mainFrame()->evaluateJavaScript(m_jQuery);
}

void CuoEngine::_onJavaScriptCall(QString command, QString state, QString scripttime)
{
    if (!m_scriptWaitting) {
        m_scriptWaitting = true;
        enginelog()<<"Receive JavaScript Command: "<<command<<state<<scripttime;
        m_scriptCommand = command;
        m_scriptState = state.split(QRegExp("[:]"),QString::SkipEmptyParts);
        m_scriptTimer->start(1000*scripttime.toInt());
    }
    else
    {
        enginelog()<<"JavaScript Command Waitting but receive: "<<command<<state<<scripttime;
    }
}
void CuoEngine::_javaScriptCall()
{
    if (!m_scriptWaitting) {
        return;
    }
	enginelog() << "Invoke JavaScript Command: " << m_scriptCommand;
	m_scriptWaitting = false;
    m_scriptTimer->stop();
	if (m_scriptCommand == "next") {
		m_step += m_scriptState[0].toInt();
		_step();
	}
	else if (m_scriptCommand == "click") {
		_adjustScreen();
		QString element;
		for (int i = 0; i < m_scriptState.count() - 1; ++i)
		{
			element.append(m_scriptState[i]);
			if (i < m_scriptState.count() - 2)
				element.append(":");
		}
		m_webview->click(element);
		if (m_scriptState.last().toInt() > 0)
		{
			m_step += m_scriptState.last().toInt();
			_step();
		}
	}
	else if (m_scriptCommand == "clickXY") {
		_adjustScreen();
		QPoint p(m_scriptState[0].toInt(), m_scriptState[1].toInt());
		m_webview->click(p);
		if (m_scriptState.count() > 2 && m_scriptState.last().toInt() > 0)
		{
			m_step += m_scriptState.last().toInt();
			_step();
		}
	}
	else if (m_scriptCommand == "input") {
		_adjustScreen();
		QString text = m_scriptState[0];
		m_inputStepCount = 1;
		if (m_scriptState.count() > 1)
		{
			m_inputStepCount = m_scriptState[1].toInt();
		}
		m_webview->input(text);
	}
	else if (m_scriptCommand == "end") {
        _complete(m_scriptState[0], m_scriptState[1]);
    }
	 
}
void CuoEngine::_onScriptButtonClicked()
{
    this->Stop();
    newLabor("debug", m_scriptTextEdit->toPlainText());
    this->Start();
}
