﻿#ifndef CUOREGISTERWINDOW_H
#define CUOREGISTERWINDOW_H

#include <QWidget>

namespace Ui {
class CuoRegisterWindow;
}

class CuoRegisterWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CuoRegisterWindow(QWidget *parent = 0);
    ~CuoRegisterWindow();

signals:
    void aboutToRegister();
    void aboutToClose();
    void aboutToLogin();

private slots:
    void displayError(QString title, QString msg);
    void _register();
	void labeldisClick();
    void _onLoginButtonClick();

private:
    Ui::CuoRegisterWindow *ui;
};

#endif // CUOREGISTERWINDOW_H
