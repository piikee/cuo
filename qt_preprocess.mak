#############################################################################
# Makefile for building: Core.app/Contents/MacOS/Core
# Generated by qmake (3.0) (Qt 5.3.2)
# Project:  Core.pro
# Template: app
# Command: /Users/lambda/Qt5.3.2/5.3/clang_64/bin/qmake -spec macx-xcode -o Core/project.pbxproj Core.pro
#############################################################################

MAKEFILE      = project.pbxproj

MOC       = /Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc
UIC       = /Users/lambda/Qt5.3.2/5.3/clang_64/bin/uic
LEX       = flex
LEXFLAGS  = 
YACC      = yacc
YACCFLAGS = -d
DEFINES       = -DQT_NO_DEBUG -DQT_WEBKITWIDGETS_LIB -DQT_WEBKIT_LIB -DQT_WIDGETS_LIB -DQT_NETWORK_LIB -DQT_GUI_LIB -DQT_CORE_LIB
INCPATH       = -I../../../Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I. -Iinclude -I../../../Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Versions/5/Headers -I../../../Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Versions/5/Headers -I../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers -I../../../Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Versions/5/Headers -I../../../Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Versions/5/Headers -I../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers -I. -I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk/System/Library/Frameworks/OpenGL.framework/Versions/A/Headers -I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk/System/Library/Frameworks/AGL.framework/Headers -I. -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib
DEL_FILE  = rm -f
MOVE      = mv -f

IMAGES = 
PARSERS =
preprocess: $(PARSERS) compilers
clean preprocess_clean: parser_clean compiler_clean

parser_clean:
mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

check: first

compilers: qrc_jquery.cpp moc_cuowebview.cpp moc_cuonetio.cpp moc_cuoengine.cpp\
	 moc_xor.cpp moc_cuowindowmanager.cpp moc_cuoapplication.cpp\
	 moc_cuouserwindow.cpp moc_cuologinwindow.cpp moc_cuoregisterwindow.cpp\
	 moc_cuopower.cpp moc_cuouser.cpp ui_cuoregisterwindow.h ui_cuouserwindow.h ui_cuologinwindow.h
compiler_objective_c_make_all:
compiler_objective_c_clean:
compiler_rcc_make_all: qrc_jquery.cpp
compiler_rcc_clean:
	-$(DEL_FILE) qrc_jquery.cpp
qrc_jquery.cpp: jquery.qrc
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/rcc -name jquery jquery.qrc -o qrc_jquery.cpp

compiler_moc_header_make_all: moc_cuowebview.cpp moc_cuonetio.cpp moc_cuoengine.cpp moc_xor.cpp moc_cuowindowmanager.cpp moc_cuoapplication.cpp moc_cuouserwindow.cpp moc_cuologinwindow.cpp moc_cuoregisterwindow.cpp moc_cuopower.cpp moc_cuouser.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_cuowebview.cpp moc_cuonetio.cpp moc_cuoengine.cpp moc_xor.cpp moc_cuowindowmanager.cpp moc_cuoapplication.cpp moc_cuouserwindow.cpp moc_cuologinwindow.cpp moc_cuoregisterwindow.cpp moc_cuopower.cpp moc_cuouser.cpp
moc_cuowebview.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QtWidgets \
		../../../Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Versions/5/Headers/QtWebKitWidgets \
		../../../Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Versions/5/Headers/QtWebKitDepends \
		cuowebview.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuowebview.h -o moc_cuowebview.cpp

moc_cuonetio.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QObject \
		../../../Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Versions/5/Headers/QTcpSocket \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QSharedPointer \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonDocument \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonObject \
		cuoproto.h \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QString \
		cuonetio.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuonetio.h -o moc_cuonetio.cpp

moc_cuoengine.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QWidget \
		../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QPlainTextEdit \
		../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QPushButton \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonArray \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonDocument \
		cuolabor.h \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QMap \
		cuoengine.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuoengine.h -o moc_cuoengine.cpp

moc_xor.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QObject \
		xor.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib xor.h -o moc_xor.cpp

moc_cuowindowmanager.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QWidget \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonObject \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonArray \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonDocument \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QMap \
		cuowindowmanager.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuowindowmanager.h -o moc_cuowindowmanager.cpp

moc_cuoapplication.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QApplication \
		cuoapplication.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuoapplication.h -o moc_cuoapplication.cpp

moc_cuouserwindow.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QObject \
		../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QWidget \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QJsonDocument \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QList \
		../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QSlider \
		cuouserwindow.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuouserwindow.h -o moc_cuouserwindow.cpp

moc_cuologinwindow.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QWidget \
		cuologinwindow.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuologinwindow.h -o moc_cuologinwindow.cpp

moc_cuoregisterwindow.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Versions/5/Headers/QWidget \
		cuoregisterwindow.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuoregisterwindow.h -o moc_cuoregisterwindow.cpp

moc_cuopower.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QObject \
		cuopower.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuopower.h -o moc_cuopower.cpp

moc_cuouser.cpp: ../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QObject \
		../../../Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Versions/5/Headers/QMap \
		cuouser.h
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/moc $(DEFINES) -D__APPLE__ -D__GNUC__=4 -I/Users/lambda/Qt5.3.2/5.3/clang_64/mkspecs/macx-clang -I/Users/lambda/Workspace/Cuo/Core -I/Users/lambda/Workspace/Cuo/Core/include -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKitWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWebKit.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtNetwork.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtGui.framework/Headers -I/Users/lambda/Qt5.3.2/5.3/clang_64/lib/QtCore.framework/Headers -F/Users/lambda/Qt5.3.2/5.3/clang_64/lib cuouser.h -o moc_cuouser.cpp

compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all: ui_cuoregisterwindow.h ui_cuouserwindow.h ui_cuologinwindow.h
compiler_uic_clean:
	-$(DEL_FILE) ui_cuoregisterwindow.h ui_cuouserwindow.h ui_cuologinwindow.h
ui_cuoregisterwindow.h: cuoregisterwindow.ui
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/uic cuoregisterwindow.ui -o ui_cuoregisterwindow.h

ui_cuouserwindow.h: cuouserwindow.ui
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/uic cuouserwindow.ui -o ui_cuouserwindow.h

ui_cuologinwindow.h: cuologinwindow.ui
	/Users/lambda/Qt5.3.2/5.3/clang_64/bin/uic cuologinwindow.ui -o ui_cuologinwindow.h

compiler_rez_source_make_all:
compiler_rez_source_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_rcc_clean compiler_moc_header_clean compiler_uic_clean 

