﻿#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H
#define REG_RUN "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run"//开机启动项
#include <QWidget>

namespace Ui {
class CuoLoginWindow;
}

class CuoLoginWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CuoLoginWindow(QWidget *parent = 0);
    ~CuoLoginWindow();
	void setAutoStart(bool is_auto_start);//开机启动项
    void tryLogin();
	void startconfig();
	void savenamepwd();
signals:
    void aboutToClose();
    void aboutToLogin();
    void aboutToRegister();

private slots:
    void displayError(QString title, QString msg);
    void onLoginClick();
    void _login();
    void _onRegisterButtonClick();

private:
    Ui::CuoLoginWindow *ui;
    QString m_name;
    QString m_password;
};

#endif // LOGINWINDOWS_H
