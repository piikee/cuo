﻿#include "cuolabor.h"
#include "cuoproto.h"
#include "cuonetio.h"
#include "cuopower.h"
#include <QtWidgets>
#include <QRegExp>

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

int ILaborStep::getSteptime() const
{
	return m_steptime;
}
void ILaborStep::setSteptime(int t)
{
	m_steptime = t;
}
int ILaborStep::getStepcount() const
{
	return m_stepcount;
}
void ILaborStep::setStepcount(int t)
{
	m_stepcount = t;
}
int ILaborStep::getIgnoreCompletedEvent() const
{
	return m_ignoreCompletedEvent;
}
void ILaborStep::setIgnoreCompletedEvent(int t)
{
	m_ignoreCompletedEvent = t;
}

const char* LaborURL::getType()
{
    return "URL";
}
const char* LaborJAVASCRIPT::getType()
{
    return "JAVASCRIPT";
}
bool CuoLabor::isOK() const
{
    return m_ok;
}
int CuoLabor::getInittime() const
{
    return m_inittime;
}
int CuoLabor::getTotaltime() const
{
    return m_totaltime;
}
int CuoLabor::getStepCount() const
{
    return m_steps.count();
}
ILaborStep* CuoLabor::getStep(int s)
{
    if (s<m_steps.count()) {
        return m_steps[s];
    }
    return NULL;
}
CuoLabor::CuoLabor(const QString& key, const QString& data)
: m_ok(false)
, m_key(key)
, m_inittime(0)
, m_totaltime(0)
{
    QJsonDocument action = QJsonDocument::fromJson(data.toUtf8());
    if(!action.object()["steps"].isArray())
        return;
    if(!action.object()["inittime"].isDouble())
        return;
    if(!action.object()["totaltime"].isDouble())
        return;
    
    m_inittime = action.object()["inittime"].toDouble();
    m_totaltime = action.object()["totaltime"].toDouble();
    
    QJsonArray steps = action.object()["steps"].toArray();
    for (auto s = steps.begin(); s != steps.end(); ++s)
    {
        if((*s).isObject() && (*s).toObject()["type"].toString() == "URL")
        {
            LaborURL* l = new LaborURL;
			l->setSteptime((*s).toObject()["steptime"].toDouble());
			l->setStepcount((*s).toObject()["stepcount"].toDouble());
			l->setIgnoreCompletedEvent((*s).toObject()["ignorecompletedevent"].toDouble());
			l->Url = (*s).toObject()["url"].toString();
            l->Ua = (*s).toObject()["ua"].toString();
            if ( (*s).toObject()["header"].isObject()) {//(*s).toObject()["ua"].isObject() &&
                QJsonObject h = (*s).toObject()["header"].toObject();
                for (auto hi = h.begin(); hi != h.end(); ++hi) {
                    l->Header[hi.key()] = hi.value().toString();
                }
            }
            m_steps.push_back(l);
        }
        if((*s).isObject() && (*s).toObject()["type"].toString() == "JAVASCRIPT")
        {
            LaborJAVASCRIPT* l = new LaborJAVASCRIPT;
            l->setSteptime( (*s).toObject()["steptime"].toDouble());
			l->setStepcount((*s).toObject()["stepcount"].toDouble());
			l->setIgnoreCompletedEvent((*s).toObject()["ignorecompletedevent"].toDouble());
			l->Script = (*s).toObject()["script"].toString();
            m_steps.push_back(l);
        }
    }
    
    qDebug()<<m_steps.count()<<m_steps.size();
    
}
CuoLabor::~CuoLabor()
{
    for (auto i = m_steps.begin(); i != m_steps.end(); ++i) {
        delete *i;
    }
}
void CuoLabor::Report(const QString& success, const QString& result, const QString& ip)
{
    PROTO_NAME(report) reportData;
    reportData.key = m_key;
    reportData.proxy_ip = ip;
    reportData.success = success;
    reportData.result = result;
	CuoNetIO::getInstance()->send(&reportData);
	CuoPower::getInstance()->RefreshProxy(success != "888");
}
void CuoLabor::Report(const QString& result)
{
    PROTO_NAME(report) reportData;
    reportData.key = m_key;
    reportData.result = result;
    CuoNetIO::getInstance()->send(&reportData);
}