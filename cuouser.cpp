﻿//
//  cuouser.cpp
//  Core
//
//  Created by Yiwei Wang on 11/2/14.
//
//

#include "cuouser.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

//------------------------------------------------
CuoUserData* CuoUserData::pInstance = NULL;

CuoUserData* CuoUserData::getInstance()
{
    if(NULL == pInstance)
    {
        pInstance = new CuoUserData();
        if(!pInstance->isInited())
        {
            pInstance->init();
        }
    }
    return pInstance;
}

void CuoUserData::Release()
{
    if(NULL != pInstance)
    {
        delete pInstance;
        pInstance = NULL;
    }
}

bool CuoUserData::isInited() const
{
    return m_inited;
}

void CuoUserData::init()
{
    m_inited = true;
}

CuoUserData::CuoUserData()
: m_inited(false)
{
}


CuoUserData::~CuoUserData()
{
}

void CuoUserData::setData(const QString& name, const QString& score, const QString& today_score, const QString& ip, const QString& role)
{
    m_name          = name;
    m_score         = score;
    m_today_score   = today_score;
    m_ip            = ip;
	m_role			= role;
}
QString CuoUserData::getName()
{
    return m_name;
}
QString CuoUserData::getScore()
{
    return m_score;
}
QString CuoUserData::getTodayScore()
{
    return m_today_score;
}
QString CuoUserData::getIp()
{
    return m_ip;
}
QString CuoUserData::getRole()
{
	return m_role;
}