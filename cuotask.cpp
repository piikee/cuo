﻿

#include "cuotask.h"

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif


CuoTask::CuoTask(const QString& key, const QString& type, const QString& start_day, const QString& end_day, const QString& keyword, const QString& keyword_b, const QString& shop, const QString& day_ip_count, const QString& total_completed_count, const QString& day_completed_ip_count)
: m_key(key)
, m_type(type)
, m_start_day(start_day)
, m_end_day(end_day)
, m_keyword(keyword)
, m_keyword_b(keyword_b)
, m_shop(shop)
, m_day_ip_count(day_ip_count)
, m_total_completed_count(total_completed_count)
, m_day_completed_ip_count(day_completed_ip_count)
{
    
}
CuoTask::~CuoTask()
{
    
}
QString CuoTask::getKey()
{
    return m_key;
}
QString CuoTask::getKeyword()
{
    return m_keyword;
}

QString CuoTask::getKeyword_b()
{
	return m_keyword_b;
}

QString CuoTask::getShop()
{
	return m_shop;
}
QString CuoTask::getStartDay()
{
    return m_start_day;
}
QString CuoTask::getEndDay()
{
    return m_end_day;
}
QString CuoTask::getType()
{
    return m_type;
}
QString CuoTask::getDayIpCount()
{
	return m_day_ip_count;
}
QString CuoTask::getTotalCompletedCount()
{
	return m_total_completed_count;
}
QString CuoTask::getDayCompletedIpCount()
{
	return m_day_completed_ip_count;
}
//------------------------------------------------------
//------------------------------------------------
CuoTaskList* CuoTaskList::pInstance = NULL;

CuoTaskList* CuoTaskList::getInstance()
{
    if(NULL == pInstance)
    {
        pInstance = new CuoTaskList();
        if(!pInstance->isInited())
        {
            pInstance->init();
        }
    }
    return pInstance;
}

void CuoTaskList::Release()
{
    if(NULL != pInstance)
    {
        delete pInstance;
        pInstance = NULL;
    }
}

bool CuoTaskList::isInited() const
{
    return m_inited;
}

void CuoTaskList::init()
{
    m_inited = true;
}


CuoTaskList::CuoTaskList()
: m_inited(false)
{
    
}

CuoTask* CuoTaskList::getTask(const QString& t)
{
    auto it = m_tasklist.find(t);
    if(it!=m_tasklist.end())
    {
        return *it;
    }
    return NULL;
}

void CuoTaskList::update(CuoTask* t)
{
    auto it = m_tasklist.find(t->getKey());
    if(it!=m_tasklist.end())
    {
        delete (*it);
        m_tasklist.remove(t->getKey());
    }
    m_tasklist.insert(t->getKey(), t);
}

CuoTaskList::~CuoTaskList()
{
    for (auto it = m_tasklist.begin(); it != m_tasklist.end(); ++it) {
        delete *it;
    }
}
