﻿#include "cuowindowmanager.h"
#include "cuologinwindow.h"
#include "cuoregisterwindow.h"
#include "cuouserwindow.h"
#include "cuonetio.h"
#include "cuoengine.h"
#include <QtWidgets>
#include "cuoapplication.h"
#include "cuouser.h"
#include "cuotask.h"

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif

static CuoWindowManager* cuoWindowManager = NULL;
void onProtoUnlock(IProto* p)
{
    Q_UNUSED(p)
    cuoWindowManager->setLock(false);
}
void onProtoMessage(IProto* p)
{
    auto m = dynamic_cast< PROTO_NAME_POINTER(message) >(p);
    if (m != NULL) {
        cuoWindowManager->showError(m->title, m->msg);
    }
}
void onProtoUser(IProto* p)
{
    auto m = dynamic_cast< PROTO_NAME_POINTER(user) >(p);
    if (m != NULL) {
        CuoUserData::getInstance()->setData(m->name,m->score,m->today_score,m->ip, m->role);
        cuoWindowManager->onLogin();
    }
}
void onProtoTask(IProto* p)
{
    auto m = dynamic_cast< PROTO_NAME_POINTER(mytask) >(p);
    if (m != NULL) {
		CuoTask* t = new CuoTask(m->key, m->type, m->start_day, m->end_day, m->keyword, m->keyword_b, m->shop, m->day_ip_count, m->total_completed_count, m->day_completed_ip_count);
        CuoTaskList::getInstance()->update(t);
        cuoWindowManager->onTaskUpdate(m->key);
    }
}
void onProtoLogout(IProto* p)
{
    auto m = dynamic_cast< PROTO_NAME_POINTER(logout) >(p);
    if (m != NULL) {
        cuoWindowManager->showError(m->title, m->msg);
        cuoWindowManager->startLogin(false);
    }
}
void onProtoQueryResult(IProto* p)
{
	auto m = dynamic_cast< PROTO_NAME_POINTER(queryresult) >(p);
	if (m != NULL) {
		cuoWindowManager->onVipUpdate(m->score, m->error);
	}
}

//-------------------------------------------------

CuoWindowManager::CuoWindowManager()
  : QWidget(NULL)
  , m_loginState(Logout)
  , m_loginWindow(NULL)
  , m_registerWindow(NULL)
  , m_userWindow(NULL)
  , m_inited(false)
  , m_isProxy(false)
{
    setObjectName("windowsManager");
}

void CuoWindowManager::init()
{
    if (m_inited) {
        return;
    }
    m_inited = true;
    cuoWindowManager = this;
	 
    CuoNetIO* net = CuoNetIO::getInstance();
    IProto* p_unlock = new PROTO_NAME(unlock);
    net->registerProto(p_unlock, onProtoUnlock);

    IProto* p_msg = new PROTO_NAME(message);
    net->registerProto(p_msg, onProtoMessage);
    
    IProto* p_user = new PROTO_NAME(user);
    net->registerProto(p_user, onProtoUser);
    
    IProto* p_task = new PROTO_NAME(mytask);
    net->registerProto(p_task, onProtoTask);

	IProto* p_logout = new PROTO_NAME(logout);
	net->registerProto(p_logout, onProtoLogout);

	IProto* p_queryresult = new PROTO_NAME(queryresult);
	net->registerProto(p_queryresult, onProtoQueryResult);

	startLogin(false);
}

void CuoWindowManager::setLoginState(LoginState s)
{
    switch (m_loginState)
    {
    case Logout:
        {
            //DW_ASSERT(s != Logined);
            m_loginState = s;
        }
        break;
    case Logining:
        {
            m_loginState = s;
            if (s == Logined)
            {
                //_emitLoginStatus("loginSuccessed");
            }
            else if (s == Logout)
            {
                //_emitLoginStatus("loginFailed");
            }
        }
        break;
    case Logined:
        {
            //DW_ASSERT(s != Logining);
            m_loginState = s;
            if (s == Logout)
            {
                //_emitLoginStatus("logout");
            }
            else if(s == Offline)
            {
            }
        }
        break;
    case Offline:
        m_loginState = s;
        if (s == Logined)
        {
        }
        break;
    }
}

void CuoWindowManager::showRegisterWindow()
{
    _showRegisterWindow();
}

void CuoWindowManager::showLoginWindow()
{
    _showLoginWindow();
}

void CuoWindowManager::showError(QString title, QString msg)
{
    QMessageBox::information(this, title, msg);
}

void CuoWindowManager::_hideWindow()
{
    if (m_loginWindow) {
        m_loginWindow->hide();
    }
    if (m_registerWindow) {
        m_registerWindow->hide();
    }
    if (m_userWindow) {
        m_userWindow->hide();
    }
}


bool CuoWindowManager::_activateWindow(QWidget *window)
{
    if (window->isHidden()) {
        _hideWindow();
        window->show();
    }
    //window->activateWindow();
    // clear iconic flag
    //startUpOptions( getStartUpOptions() & ~StartUp_iconic);
    return true;
}
int CuoWindowManager::_createLoginWindow()
{
    if(m_loginWindow)
    {
        return 0;
    }
    m_loginWindow = new CuoLoginWindow();
   
    connect(m_loginWindow, SIGNAL(aboutToRegister()), this, SLOT(showRegisterWindow()));
    //connect(m_loginWindow, SIGNAL(aboutToLogin()), SLOT(on_loginWindow_aboutToLogin()));
    return 1;
}

void CuoWindowManager::_showLoginWindow()
{
    if( _createLoginWindow() )
    {
    }
   _activateWindow(m_loginWindow);
}

void CuoWindowManager::startLogin(bool trylogin)
{
     _showLoginWindow();
   if (trylogin) {
        m_loginWindow->tryLogin();
    }
}

int CuoWindowManager::_createRegisterWindow()
{
    if(m_registerWindow)
    {
        return 0;
    }
    m_registerWindow = new CuoRegisterWindow();
    
    //connect(m_registerWindow, SIGNAL(aboutToClose()), SLOT(on_registerWindow_aboutToClose()));
    connect(m_registerWindow, SIGNAL(aboutToLogin()), SLOT(showLoginWindow()));
    
    return 1;
}

void CuoWindowManager::_showRegisterWindow()
{
    if( _createRegisterWindow() )
    {
    }
    _activateWindow(m_registerWindow);
}

int CuoWindowManager::_createUserWindow()
{
    if(m_userWindow)
    {
        return 0;
    }
    m_userWindow = new CuoUserWindow(); 
 
    //connect(m_registerWindow, SIGNAL(aboutToClose()), SLOT(on_registerWindow_aboutToClose()));
    //connect(m_registerWindow, SIGNAL(aboutToLogin()), SLOT(showLoginWindow()));
    
    return 1;
}



void CuoWindowManager::_showUserWindow()
{
    if( _createUserWindow() )
    {
    }
    _activateWindow(m_userWindow);
}

void CuoWindowManager::setLock(bool locked)
{
    if (m_loginWindow) {
        m_loginWindow->setEnabled(!locked);
    }
    if (m_registerWindow) {
        m_registerWindow->setEnabled(!locked);
    }
    if (m_userWindow) {
        m_userWindow->setEnabled(!locked);
    }
}

void CuoWindowManager::iamProxy()
{
	m_isProxy = true;
}
void CuoWindowManager::onNetworkConnected()
{
    setLock(false);
    startLogin(true);
	if (m_isProxy)
	{
		PROTO_NAME(iamproxy) iamproxyData;
		CuoNetIO::getInstance()->send(&iamproxyData);
	}
}

void CuoWindowManager::onNetworkDisconnected()
{
    setLock(true);
}

void CuoWindowManager::onLogin()
{
    _showUserWindow();
    m_userWindow->refreshUserInfo();
}
void CuoWindowManager::onTaskUpdate(const QString& key)
{
    m_userWindow->refreshTaskInfo(key);
}

void CuoWindowManager::onVipUpdate(const QString& score, const QString& error)
{
	if (score == "-1")
		m_userWindow->refreshVipInfo(error);
	else
		m_userWindow->refreshVipInfo(QString("当前积分%1").arg(score));
}

