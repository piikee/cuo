﻿#ifndef CUOWINDOWMANAGER_H
#define CUOWINDOWMANAGER_H

#include <QWidget>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMap>

class CuoLoginWindow;
class CuoRegisterWindow;
class CuoUserWindow;
class CuoNetIO;
class CuoEngine;

class CuoWindowManager : public QWidget
{
    Q_OBJECT
public:
    explicit CuoWindowManager();
    void init();

    enum LoginState
    {
        Logout,
        Logining,
        Logined,
        Offline,
    };

    void setLoginState(LoginState s);

    void startLogin(bool trylogin);
	void iamProxy();

signals:

public slots:
    void setLock(bool);
    
    void onNetworkConnected();
    void onNetworkDisconnected();
    void onLogin();
    void onTaskUpdate(const QString&);
	void onVipUpdate(const QString& score, const QString& error);

    void showRegisterWindow();
    void showLoginWindow();
    void showError(QString title, QString msg);

    

private:
    int _createLoginWindow();
    int _createRegisterWindow();
    int _createUserWindow();
    
    void _hideWindow();

    bool _activateWindow(QWidget *window);
//    void _activateWindowAnyway(DuiFrameWindow *window);

    void _showLoginWindow();
    void _showRegisterWindow();
    void _showUserWindow();

    //void _showMainframe(ShowCommand showCommand);
    //void _destroyMainframe();

    //void _switchToLoginWindow(LoginWindowShowReason reason, IPropBagPtrCR errInfo);
    //void _switchToMainFrame();

private:
    LoginState          m_loginState;
    CuoLoginWindow *    m_loginWindow;
    CuoRegisterWindow * m_registerWindow;
    CuoUserWindow *     m_userWindow;

    QString     m_name;
    QString     m_password;
    bool        m_inited;
	bool		m_isProxy;
};

#endif // CUOWINDOWMANAGER_H
