﻿#include <QTime>
#include <QNetworkProxy>
#include <QFile>
#include <QtDebug>
#include <QtWidgets>
#include <qcommandlineparser>
#include <qprocess.h>
#include <QDialog>
#include <qtcpserver.h>
#include "cuoapplication.h"
#include "cuowindowmanager.h"
#include "cuonetio.h"
#include "cuoproto.h"
#include "cuopower.h"
#include "xor.h"

#if _MSC_VER >= 1600    // VC2010
#pragma execution_character_set("utf-8")
#endif


QFile* logFile;
void cuoMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & str)
{
	QString txt;
	switch (type) {
	case QtDebugMsg:
		txt = QString("Debug: %1").arg(str);
		break;
	case QtWarningMsg:
		txt = QString("Warning: %1").arg(str);
		break;
	case QtCriticalMsg:
		txt = QString("Critical: %1").arg(str);
		break;
	case QtFatalMsg:
		txt = QString("Fatal: %1").arg(str); 
		break;
	}
	QTextStream ts(logFile);
	ts << txt << endl;
}

CuoApplication::CuoApplication( int argc, char *argv[] )
: QApplication( argc, argv )
, m_mutexInstance(0)
, m_windowManager(NULL)
, m_tcpServerConnection(NULL)
, m_slaverRestareTimer(NULL)
, m_slaverEngineTimer(NULL)
, m_isEnableProxy(false)
{
	QStringList arguments = QCoreApplication::arguments();
	m_isSlaver = false;
	for (auto s = arguments.begin(); s != arguments.end(); ++ s)
	{
		if (*s == "slaver")
		{
			m_isSlaver = true;
			m_port = (s + 1)->toInt();
			break;
		}
	}
	if (m_isSlaver)
	{
		Slaver();
	}
	else
	{
		Master();
	}
}

bool CuoApplication::isSlaver()
{
	return m_isSlaver;
}
void CuoApplication::Master()
{
	m_tcpServer = new QTcpServer;
	m_slaverRestareTimer = new QTimer();
	m_slaverEngineTimer = new QTimer();
	connect(m_slaverRestareTimer, SIGNAL(timeout()), this, SLOT(slaverRestareTimeout()));
	connect(m_slaverEngineTimer, SIGNAL(timeout()), this, SLOT(engineStart()));
	connect(m_tcpServer, SIGNAL(newConnection()),
		this, SLOT(acceptConnection()));

	if (!m_tcpServer->isListening() && !m_tcpServer->listen()) {
		qDebug() << "Error m_tcpServer listen: " << m_tcpServer->errorString();
	}
	m_port = m_tcpServer->serverPort();
	QString program = QCoreApplication::applicationFilePath();
	QStringList arguments;
	arguments << "slaver" << QString("%1").arg(m_port); 
	m_slaverProcess = new QProcess();
	m_slaverProcess->start(program, arguments);

	setEndPoint("c.piikee.net",8888, "12345678");
	m_netIO = CuoNetIO::getInstance();
	m_windowManager = new CuoWindowManager();
	QFont font("Time", 10, QFont::Normal, false);
	setFont(font);

}

void onProtoLaborToSlaver(IProto* p)
{
	auto m = dynamic_cast< PROTO_NAME_POINTER(labor) >(p);
	if (m != NULL) {
		cuoApp()->sendSlaver(m);
	}
}
void onProtoExpireProxyToSlaver(IProto* p)
{
	auto m = dynamic_cast< PROTO_NAME_POINTER(expireproxy) >(p);
	if (m != NULL) {
		cuoApp()->sendSlaver(m);
	}
}

void CuoApplication::acceptConnection()
{
	if (m_tcpServerConnection)
		delete m_tcpServerConnection;
	m_tcpServerConnection = m_tcpServer->nextPendingConnection();
	connect(m_tcpServerConnection, SIGNAL(readyRead()),
		this, SLOT(receiveSlaverData()));
	connect(m_tcpServerConnection, SIGNAL(error(QAbstractSocket::SocketError)),
		this, SLOT(localConnectionError(QAbstractSocket::SocketError)));
	connect(m_tcpServerConnection, SIGNAL(disconnected()), this, SLOT(masterDisconnected()));
	m_tcpServer->close();
	m_receiveSlaverBlockSize = 0;

	m_slaverRestareTimer->start(120* 60 * 1000);
	m_slaverEngineTimer->start(2 * 1000);
}

void CuoApplication::engineStart()
{
	m_slaverEngineTimer->stop();
	PROTO_NAME(report) reportData;
	reportData.key = "info";
	reportData.success = "999";
	reportData.result = "Engine Start";
	CuoNetIO::getInstance()->send(&reportData);
}

void CuoApplication::slaverRestareTimeout()
{
	m_slaverRestareTimer->stop();
	if (m_tcpServerConnection)
		m_tcpServerConnection->close();
}
void CuoApplication::receiveSlaverData()
{
	QDataStream in(m_tcpServerConnection);
	do
	{
		if (m_receiveSlaverBlockSize == 0) {
			if (m_tcpServerConnection->bytesAvailable() < (int)sizeof(quint16))
				return;
			in >> m_receiveSlaverBlockSize;
		}

		if (m_tcpServerConnection->bytesAvailable() < m_receiveSlaverBlockSize)
			return;

		QByteArray buffer(m_receiveSlaverBlockSize, Qt::Uninitialized);
		in.readRawData(buffer.data(), m_receiveSlaverBlockSize);
		QByteArray response = QByteArray::fromBase64(buffer);
		response = Xor::convertData("12345678", response);
		cuolog() << "receive data: " << response;

		m_netIO->rawSend(response);
		m_receiveSlaverBlockSize = 0;
	} while (true);
}

void CuoApplication::localConnectionError(QAbstractSocket::SocketError socketError)
{
	switch (socketError) {
	case QAbstractSocket::RemoteHostClosedError:
		break;
	case QAbstractSocket::HostNotFoundError:
		break;
	case QAbstractSocket::ConnectionRefusedError:
		break;
	default:
		;
	}
}

void CuoApplication::sendSlaver(IProto* proto)
{
	if (!m_tcpServerConnection || m_tcpServerConnection->state() != QAbstractSocket::ConnectedState)
        return;    
    QString data;
    proto->serialize(data);
    QString send = QString("{\"command\":\"%1\",\"data\":%2}").arg(proto->getProtoName()).arg(data);

    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setByteOrder(QDataStream::BigEndian);
    out.setVersion(QDataStream::Qt_4_0);
    out << (quint16)0;
    QString response = Xor::convertData("12345678", send.toUtf8()).toBase64();
	QByteArray rep = response.toUtf8();//.toLocal8Bit();
    cuolog()<<"send data: "<<send;
    //cuolog()<<"xor data: "<<rep;
    out.writeRawData(rep.data(), rep.size());
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));
	m_tcpServerConnection->write(block);
}

void CuoApplication::Slaver()
{
	m_tcpClient = new QTcpSocket;
	//this->EnableLog();
	connect(m_tcpClient, SIGNAL(connected()), this, SLOT(startTransfer()));
	connect(m_tcpClient, SIGNAL(bytesWritten(qint64)),
		this, SLOT(updateClientProgress(qint64)));
	connect(m_tcpClient, SIGNAL(error(QAbstractSocket::SocketError)),
		this, SLOT(displayError(QAbstractSocket::SocketError)));
	//m_tcpClient->connectToHost(QHostAddress::LocalHost, m_port);

	setEndPoint("127.0.0.1", m_port, "12345678");
	m_netIO = CuoNetIO::getInstance();
	m_windowManager = new CuoWindowManager();
	QFont font("Time", 10, QFont::Normal, false);
	setFont(font);
	m_netIO->start(); 
	m_power = CuoPower::getInstance();
}
CuoApplication::~CuoApplication()
{
    //DW_SAFE_DELETE(m_windowManager);	//m_windowManager会析构mainframe，mainframe析构中会deactiveCheckPoint(KLoginCheckpoint);

}

bool CuoApplication::start()
{
	m_isEnableProxy = false;    //是否代理开关
	if (m_isSlaver)
	{
		m_power->EnableProxy(m_isEnableProxy);
		m_power->ShowWebView(false);
		m_power->init();
		connect(m_netIO, SIGNAL(sigDisconnected()), this, SLOT(slaverDisconnected()));
		return true;
	}
    //_processArguments();
    m_windowManager->init(); 
    connect(m_netIO, SIGNAL(sigConnected()), m_windowManager, SLOT(onNetworkConnected()));
    connect(m_netIO, SIGNAL(sigDisconnected()), m_windowManager, SLOT(onNetworkDisconnected()));
    m_netIO->start();
	//this->EnableLog();
	IProto* p_labor = new PROTO_NAME(labor);
	m_netIO->registerProto(p_labor, onProtoLaborToSlaver);
	IProto* p_expireproxy = new PROTO_NAME(expireproxy);
	m_netIO->registerProto(p_expireproxy, onProtoExpireProxyToSlaver);
	if (m_isEnableProxy)
	{
		m_windowManager->iamProxy();
	}
	return true;
}

void CuoApplication::slaverDisconnected()
{
	QCoreApplication::quit();
}

void CuoApplication::restartSlaver()
{
	m_slaverProcess->kill(); 
	m_slaverProcess->waitForFinished();
	if (!m_tcpServer->isListening() && !m_tcpServer->listen()) {
		qDebug() << "Error m_tcpServer listen: " << m_tcpServer->errorString();
	}
	m_port = m_tcpServer->serverPort();
	QString program = QCoreApplication::applicationFilePath();
	QStringList arguments;
	arguments << "slaver" << QString("%1").arg(m_port);
	m_slaverProcess->start(program, arguments);
}

void CuoApplication::masterDisconnected()
{
	restartSlaver();
}

void CuoApplication::EnableLog()
{
	logFile = new QFile("cuo.log");
	logFile->open(QIODevice::WriteOnly | QIODevice::Append);
	qInstallMessageHandler(cuoMessageHandler);
}

void CuoApplication::setCurrentUserUid( qint32 uid )
{

}


CuoWindowManager * CuoApplication::windowManager() const
{
    return m_windowManager;
}

CuoNetIO * CuoApplication::netIO()
{
    return m_netIO;
}

CuoApplication *cuoApp()
{
    return static_cast<CuoApplication *>(QCoreApplication::instance());
}

