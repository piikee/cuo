﻿#ifndef CUOTASK_H
#define CUOTASK_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMap>


class CuoTask
{
public:
	CuoTask(const QString& key, const QString& type, const QString& start_day, const QString& end_day, const QString& keyword, const QString& keyword_b, const QString& shop, const QString& day_ip_count, const QString& total_completed_count, const QString& day_completed_ip_count);
    virtual ~CuoTask();
    QString getKey();
    QString getType();
    QString getStartDay();
    QString getEndDay();
	QString getKeyword(); 
	QString getKeyword_b();
	QString getShop();
	QString getDayIpCount();
	QString getTotalCompletedCount();
	QString getDayCompletedIpCount();

private:
    QString m_key;
    QString m_type;
    QString m_start_day;
    QString m_end_day;
    QString m_keyword;
	QString m_keyword_b;
	QString m_shop;
	QString m_day_ip_count;
	QString m_total_completed_count;
	QString m_day_completed_ip_count;
};

class CuoTaskList
{
public:
    static CuoTaskList* getInstance();
    
    static void Release();
    
    virtual void init();
    
    virtual bool isInited() const;

    void update(CuoTask * t);
    
    CuoTask* getTask(const QString& key);
    
private:
    CuoTaskList();
    virtual ~CuoTaskList();

private:
    static CuoTaskList* pInstance;
    bool m_inited;

    QMap<QString, CuoTask*> m_tasklist;
};

#endif // CUOLABOR_H
